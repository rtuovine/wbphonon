#include "Main.hpp"
void WriteResults(int Nc, int Nt, double dt, cx_cube D_lss, cx_mat D_lss_ss, cx_mat D_lss_ss2, cx_mat jQLeft, 
                  cx_mat jQRight, cx_vec EQ, cx_vec dEQ, cx_mat LocalT, cx_mat dLocalT,
                  cx_mat Eloc, cx_mat dEloc)
{
    int rank, size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

//  TD densities (diagonal elements of the density matrix)

    MPI_File file;
    MPI_Status status;
    MPI_Datatype num_as_string;         // Declare new datatype for storing numbers on a string
    const int nrows = Nt;               // Total number of rows to be written
    int ncols = 2*Nc+1;                 // Total number of columns to be written
    char *const fmt = "%15.8e ";        // Writing format within a row
    char *const endfmt = "%15.8e\n";    // Writing format for the end of row
    int startrow, endrow, locnrows;

    const int charspernum = 16;         // Each number is represented by this many chars

    locnrows = nrows/size;              // Local number of rows
    startrow = rank*locnrows;           // Each process starts from this row
    endrow = startrow+locnrows-1;       // Each process writes its own part
    if(rank == size-1)
    {
        endrow = nrows-1;               // The last process finalizes
        locnrows = endrow-startrow+1;
    }

    // Each number takes charspernum chars
    MPI_Type_contiguous(charspernum, MPI_CHAR, &num_as_string);
    MPI_Type_commit(&num_as_string);

    // Collect data into char buffer
    char *data_as_txt = (char*)malloc(locnrows*ncols*charspernum*sizeof(char));
    int count = 0;
    for(int i=0; i<locnrows; i++)
    {
        sprintf(&data_as_txt[count*charspernum], fmt, (locnrows*rank+i)*dt);
        count++;
        for(int j=0; j<ncols-2; j++)
        {
            sprintf(&data_as_txt[count*charspernum], fmt, real(D_lss(j,j,i)));
            count++;
        }
        sprintf(&data_as_txt[count*charspernum], endfmt, real(D_lss(ncols-2,ncols-2,i)));
        count++;
    }

    // Open the file, set the view, write, close and free datatypes
    MPI_File_open(MPI_COMM_WORLD, "output/td_density.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &file);
    int disp = locnrows*rank*ncols*charspernum*sizeof(char);
    MPI_File_set_view(file, disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
    MPI_File_write_all(file, data_as_txt, locnrows*ncols, num_as_string, &status);
    MPI_File_close(&file);
    MPI_Type_free(&num_as_string);

//  Full density matrix

    MPI_File f_file;
    MPI_Status f_status;
    MPI_Datatype f_num_as_string;
    const int f_nrows = Nt;
    int f_ncols = 2*pow(2*Nc,2)+1;
    int f_startrow, f_endrow, f_locnrows;

    f_locnrows = f_nrows/size;
    f_startrow = rank*f_locnrows;
    f_endrow = f_startrow+f_locnrows-1;
    if(rank == size-1)
    {
        f_endrow = f_nrows-1;
        f_locnrows = f_endrow-f_startrow+1;
    }

    MPI_Type_contiguous(charspernum, MPI_CHAR, &f_num_as_string);
    MPI_Type_commit(&f_num_as_string);

    char *f_data_as_txt = (char*)malloc(f_locnrows*f_ncols*charspernum*sizeof(char));
    int f_count = 0;
    for(int i=0; i<f_locnrows; i++)
    {
        sprintf(&f_data_as_txt[f_count*charspernum], fmt, (f_locnrows*rank+i)*dt);
        f_count++;
        for(int j=0; j<2*Nc; j++)
        {
            if(j<2*Nc-1)
            {
                for(int k=0; k<2*Nc; k++)
                {
                    sprintf(&f_data_as_txt[f_count*charspernum], fmt, real(D_lss(j,k,i)));
                    f_count++;
                    sprintf(&f_data_as_txt[f_count*charspernum], fmt, imag(D_lss(j,k,i)));
                    f_count++;
                }
            }
            else
            {
                for(int k=0; k<2*Nc-1; k++)
                {
                    sprintf(&f_data_as_txt[f_count*charspernum], fmt, real(D_lss(j,k,i)));
                    f_count++;
                    sprintf(&f_data_as_txt[f_count*charspernum], fmt, imag(D_lss(j,k,i)));
                    f_count++;
                }
                sprintf(&f_data_as_txt[f_count*charspernum], fmt, real(D_lss(2*Nc-1,2*Nc-1,i)));
                f_count++;
                sprintf(&f_data_as_txt[f_count*charspernum], endfmt, imag(D_lss(2*Nc-1,2*Nc-1,i)));
                f_count++;
            }
        }
    }

    MPI_File_open(MPI_COMM_WORLD, "output/td1rdm.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &f_file);
    int f_disp = f_locnrows*rank*f_ncols*charspernum*sizeof(char);
    MPI_File_set_view(f_file, f_disp, MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
    MPI_File_write_all(f_file, f_data_as_txt, f_locnrows*f_ncols, f_num_as_string, &f_status);
    MPI_File_close(&f_file);
    MPI_Type_free(&f_num_as_string);

//  SS Densities
//  (Only one row)

    if(rank == 0) 
    {
        ofstream ss_density("output/ss_density.out", ios::out);
        for(unsigned int j=0; j<2*Nc; j++)
        {
            ss_density << setprecision(5) << scientific << real(D_lss_ss(j,j)) << "\t";
        }
        ss_density << endl;
        ss_density.close();

        ofstream ss_density2("output/ss_density_beyondWBA.out", ios::out);
        for(unsigned int j=0; j<2*Nc; j++)
        {
            ss_density2 << setprecision(5) << scientific << real(D_lss_ss2(j,j)) << "\t";
        }
        ss_density2 << endl;
        ss_density2.close();

        ofstream init_density("output/init_density.out", ios::out);
        for(unsigned int j=0; j<2*Nc; j++)
        {
            init_density << setprecision(5) << scientific << real(D_lss.slice(0)(j,j)) << "\t";
        }
        init_density << endl;
        init_density.close();
    }

//  Heat currents and energies

    if(Nc > 1)
    {
        MPI_File hc_left_file;
        MPI_Status hc_left_status;
        MPI_Datatype hc_left_num_as_string;
        const int hc_left_ncols = Nc;
        int hc_left_startrow, hc_left_endrow, hc_left_locnrows;

        hc_left_locnrows = nrows/size;
        hc_left_startrow = rank*hc_left_locnrows;
        hc_left_endrow = hc_left_startrow+hc_left_locnrows-1;
        if(rank == size-1)
        {
            hc_left_endrow = nrows-1;
            hc_left_locnrows = hc_left_endrow-hc_left_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &hc_left_num_as_string);
        MPI_Type_commit(&hc_left_num_as_string);

        char *hc_left_data_as_txt = (char*)malloc(hc_left_locnrows*hc_left_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<hc_left_locnrows; i++)
        {
            sprintf(&hc_left_data_as_txt[count*charspernum], fmt, (hc_left_locnrows*rank+i)*dt);
            count++;
            for(int j=0; j<hc_left_ncols-2; j++)
            {
                sprintf(&hc_left_data_as_txt[count*charspernum], fmt, real(jQLeft(j,i)));
                count++;
            }
            sprintf(&hc_left_data_as_txt[count*charspernum], endfmt, real(jQLeft(hc_left_ncols-2,i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/heat_current_left.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &hc_left_file);
        int hc_left_disp = hc_left_locnrows*rank*hc_left_ncols*charspernum*sizeof(char);
        MPI_File_set_view(hc_left_file, hc_left_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(hc_left_file, hc_left_data_as_txt, hc_left_locnrows*hc_left_ncols, hc_left_num_as_string, &hc_left_status);
        MPI_File_close(&hc_left_file);
        MPI_Type_free(&hc_left_num_as_string);

    //

        MPI_File hc_right_file;
        MPI_Status hc_right_status;
        MPI_Datatype hc_right_num_as_string;
        const int hc_right_ncols = Nc;
        int hc_right_startrow, hc_right_endrow, hc_right_locnrows;

        hc_right_locnrows = nrows/size;
        hc_right_startrow = rank*hc_right_locnrows;
        hc_right_endrow = hc_right_startrow+hc_right_locnrows-1;
        if(rank == size-1)
        {
            hc_right_endrow = nrows-1;
            hc_right_locnrows = hc_right_endrow-hc_right_startrow+1;
        }

        MPI_Type_contiguous(charspernum, MPI_CHAR, &hc_right_num_as_string);
        MPI_Type_commit(&hc_right_num_as_string);

        char *hc_right_data_as_txt = (char*)malloc(hc_right_locnrows*hc_right_ncols*charspernum*sizeof(char));
        count = 0;
        for(int i=0; i<hc_right_locnrows; i++)
        {
            sprintf(&hc_right_data_as_txt[count*charspernum], fmt, (hc_right_locnrows*rank+i)*dt);
            count++;
            for(int j=0; j<hc_right_ncols-2; j++)
            {
                sprintf(&hc_right_data_as_txt[count*charspernum], fmt, real(jQRight(j,i)));
                count++;
            }
            sprintf(&hc_right_data_as_txt[count*charspernum], endfmt, real(jQRight(hc_right_ncols-2,i)));
            count++;
        }

        MPI_File_open(MPI_COMM_WORLD, "output/heat_current_right.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &hc_right_file);
        int hc_right_disp = hc_right_locnrows*rank*hc_right_ncols*charspernum*sizeof(char);
        MPI_File_set_view(hc_right_file, hc_right_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
        MPI_File_write_all(hc_right_file, hc_right_data_as_txt, hc_right_locnrows*hc_right_ncols, hc_right_num_as_string, &hc_right_status);
        MPI_File_close(&hc_right_file);
        MPI_Type_free(&hc_right_num_as_string);
    }

//

    MPI_File eq_file;
    MPI_Status eq_status;
    MPI_Datatype eq_num_as_string;
    const int eq_ncols = 3;
    int eq_startrow, eq_endrow, eq_locnrows;

    eq_locnrows = nrows/size;
    eq_startrow = rank*eq_locnrows;
    eq_endrow = eq_startrow+eq_locnrows-1;
    if(rank == size-1)
    {
        eq_endrow = nrows-1;
        eq_locnrows = eq_endrow-eq_startrow+1;
    }

    MPI_Type_contiguous(charspernum, MPI_CHAR, &eq_num_as_string);
    MPI_Type_commit(&eq_num_as_string);

    char *eq_data_as_txt = (char*)malloc(eq_locnrows*eq_ncols*charspernum*sizeof(char));
    count = 0;
    for(int i=0; i<eq_locnrows; i++)
    {
        sprintf(&eq_data_as_txt[count*charspernum], fmt, (eq_locnrows*rank+i)*dt);
        count++;
        sprintf(&eq_data_as_txt[count*charspernum], fmt, real(EQ(i)));
        count++;
        sprintf(&eq_data_as_txt[count*charspernum], endfmt, real(dEQ(i)));
        count++;
    }

    MPI_File_open(MPI_COMM_WORLD, "output/heat_energy.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &eq_file);
    int eq_disp = eq_locnrows*rank*eq_ncols*charspernum*sizeof(char);
    MPI_File_set_view(eq_file, eq_disp,  MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
    MPI_File_write_all(eq_file, eq_data_as_txt, eq_locnrows*eq_ncols, eq_num_as_string, &eq_status);
    MPI_File_close(&eq_file);
    MPI_Type_free(&eq_num_as_string);

//

    MPI_File loct_file;
    MPI_Status loct_status;
    MPI_Datatype loct_num_as_string;
    int loct_ncols = Nc+1;
    int loct_startrow, loct_endrow, loct_locnrows;

    loct_locnrows = nrows/size;
    loct_startrow = rank*loct_locnrows;
    loct_endrow = loct_startrow+loct_locnrows-1;
    if(rank == size-1)
    {
        loct_endrow = nrows-1;
        loct_locnrows = loct_endrow-loct_startrow+1;
    }

    MPI_Type_contiguous(charspernum, MPI_CHAR, &loct_num_as_string);
    MPI_Type_commit(&loct_num_as_string);

    char *loct_data_as_txt = (char*)malloc(loct_locnrows*loct_ncols*charspernum*sizeof(char));
    count = 0;
    for(int i=0; i<loct_locnrows; i++)
    {
        sprintf(&loct_data_as_txt[count*charspernum], fmt, (loct_locnrows*rank+i)*dt);
        count++;
        for(int j=0; j<loct_ncols-2; j++)
        {
            sprintf(&loct_data_as_txt[count*charspernum], fmt, real(LocalT(j,i)));
            count++;
        }
        sprintf(&loct_data_as_txt[count*charspernum], endfmt, real(LocalT(loct_ncols-2,i)));
        count++;
    }

    MPI_File_open(MPI_COMM_WORLD, "output/td_local_temperature.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &loct_file);
    int loct_disp = loct_locnrows*rank*loct_ncols*charspernum*sizeof(char);
    MPI_File_set_view(loct_file, loct_disp, MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
    MPI_File_write_all(loct_file, loct_data_as_txt, loct_locnrows*loct_ncols, loct_num_as_string, &loct_status);
    MPI_File_close(&loct_file);
    MPI_Type_free(&loct_num_as_string);

//

    MPI_File dloct_file;
    MPI_Status dloct_status;
    MPI_Datatype dloct_num_as_string;
    int dloct_ncols = Nc+1;
    int dloct_startrow, dloct_endrow, dloct_locnrows;

    dloct_locnrows = nrows/size;
    dloct_startrow = rank*dloct_locnrows;
    dloct_endrow = dloct_startrow+dloct_locnrows-1;
    if(rank == size-1)
    {
        dloct_endrow = nrows-1;
        dloct_locnrows = dloct_endrow-dloct_startrow+1;
    }

    MPI_Type_contiguous(charspernum, MPI_CHAR, &dloct_num_as_string);
    MPI_Type_commit(&dloct_num_as_string);

    char *dloct_data_as_txt = (char*)malloc(dloct_locnrows*dloct_ncols*charspernum*sizeof(char));
    count = 0;
    for(int i=0; i<dloct_locnrows; i++)
    {
        sprintf(&dloct_data_as_txt[count*charspernum], fmt, (dloct_locnrows*rank+i)*dt);
        count++;
        for(int j=0; j<dloct_ncols-2; j++)
        {
            sprintf(&dloct_data_as_txt[count*charspernum], fmt, real(dLocalT(j,i)));
            count++;
        }
        sprintf(&dloct_data_as_txt[count*charspernum], endfmt, real(dLocalT(dloct_ncols-2,i)));
        count++;
    }

    MPI_File_open(MPI_COMM_WORLD, "output/td_dlocal_temperature.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &dloct_file);
    int dloct_disp = dloct_locnrows*rank*dloct_ncols*charspernum*sizeof(char);
    MPI_File_set_view(dloct_file, dloct_disp, MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
    MPI_File_write_all(dloct_file, dloct_data_as_txt, dloct_locnrows*dloct_ncols, dloct_num_as_string, &dloct_status);
    MPI_File_close(&dloct_file);
    MPI_Type_free(&dloct_num_as_string);

//

    MPI_File eloc_file;
    MPI_Status eloc_status;
    MPI_Datatype eloc_num_as_string;
    int eloc_ncols = Nc+1;
    int eloc_startrow, eloc_endrow, eloc_locnrows;

    eloc_locnrows = nrows/size;
    eloc_startrow = rank*eloc_locnrows;
    eloc_endrow = eloc_startrow+eloc_locnrows-1;
    if(rank == size-1)
    {
        eloc_endrow = nrows-1;
        eloc_locnrows = eloc_endrow-eloc_startrow+1;
    }

    MPI_Type_contiguous(charspernum, MPI_CHAR, &eloc_num_as_string);
    MPI_Type_commit(&eloc_num_as_string);

    char *eloc_data_as_txt = (char*)malloc(eloc_locnrows*eloc_ncols*charspernum*sizeof(char));
    count = 0;
    for(int i=0; i<eloc_locnrows; i++)
    {
        sprintf(&eloc_data_as_txt[count*charspernum], fmt, (eloc_locnrows*rank+i)*dt);
        count++;
        for(int j=0; j<eloc_ncols-2; j++)
        {
            sprintf(&eloc_data_as_txt[count*charspernum], fmt, real(Eloc(j,i)));
            count++;
        }
        sprintf(&eloc_data_as_txt[count*charspernum], endfmt, real(Eloc(eloc_ncols-2,i)));
        count++;
    }

    MPI_File_open(MPI_COMM_WORLD, "output/td_local_energy.out", MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &eloc_file);
    int eloc_disp = eloc_locnrows*rank*eloc_ncols*charspernum*sizeof(char);
    MPI_File_set_view(eloc_file, eloc_disp, MPI_CHAR, MPI_CHAR, "native", MPI_INFO_NULL);
    MPI_File_write_all(eloc_file, eloc_data_as_txt, eloc_locnrows*eloc_ncols, eloc_num_as_string, &eloc_status);
    MPI_File_close(&eloc_file);
    MPI_Type_free(&eloc_num_as_string);
}
