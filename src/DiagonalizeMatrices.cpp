#include "Main.hpp"
void DiagonalizeMatrices(int Nc, cx_mat OmegaTilde, cx_mat alpha, cx_mat OmegaEff, 
                         cx_vec &EffEvalL, cx_mat &EffEvecL, cx_vec &EffEvalR, cx_mat &EffEvecR)
{
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    /*  These are not needed (so far)

    // OmegaTilde
    vec Eval(2*Nc);
    cx_mat Evec(2*Nc,2*Nc);
    Eval.zeros();
    Evec.zeros();
    eig_sym(Eval, Evec, OmegaTilde);

    // OmegaTilde*alpha
    cx_vec Eval_Omega_alpha(2*Nc);
    cx_mat Evec_Omega_alpha(2*Nc,2*Nc);
    Eval_Omega_alpha.zeros();
    Evec_Omega_alpha.zeros();
    eig_gen(Eval_Omega_alpha, Evec_Omega_alpha, OmegaTilde*alpha);

    */

    // OmegaEff
    eig_gen(EffEvalL, EffEvecL, OmegaEff, 'l');
    eig_gen(EffEvalR, EffEvecR, OmegaEff, 'r');

    /*  Also these are unnecessary (so far)

    // Overlaps
    cx_mat OverlapLR(2*Nc,2*Nc), OverlapRL(2*Nc,2*Nc), InvOverLR(2*Nc,2*Nc), InvOverRL(2*Nc,2*Nc);
    OverlapLR.zeros();
    OverlapRL.zeros();
    InvOverLR.zeros();
    InvOverRL.zeros();
    OverlapLR = trans(EffEvecL)*EffEvecR;
    OverlapRL = trans(EffEvecR)*EffEvecL;
    InvOverLR = inv(OverlapLR);
    InvOverRL = inv(OverlapRL);

    */

    if(rank == 0)
    {
        EffEvalL.print("\nEigenvalues of OmegaEff");
        cout << endl;
    }

}
