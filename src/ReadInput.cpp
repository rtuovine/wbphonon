#include "Main.hpp"
void ReadInput(int &systype, int &Nc, int &Nt, int &subs, int &s, int &terminal, double &kC, double &kC0, double &klambda, 
               double &klambda0, double &klambdaC, double &range, double &betaL, double &betaR, 
               double &dt, double &acc, int &sol, int &padeorder)
{

// Read file "input/parameters.in" into stream 'input' and further into corresponding variables
    string temp;
    double tmpdble;
    ifstream input, inputtmp;
    input.open("input/parameters.in");
    input >> systype; getline(input,temp);
    input >> Nc; getline(input,temp);
    input >> kC; getline(input,temp);
    input >> kC0; getline(input,temp);
    input >> klambda; getline(input,temp);
    input >> klambda0; getline(input,temp);
    input >> klambdaC; getline(input,temp);
    input >> range; getline(input,temp);
    input >> tmpdble; getline(input,temp);
    betaL = 1.0/tmpdble;
    input >> tmpdble; getline(input,temp);
    betaR = 1.0/tmpdble;
    input >> Nt; getline(input,temp);
    input >> dt; getline(input,temp);
    input >> subs; getline(input,temp);
    input >> acc; getline(input,temp);
    input >> s; getline(input,temp);
    input >> terminal; getline(input,temp);
    input >> sol; getline(input,temp);
    input >> padeorder; getline(input,temp);
    input.close();

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0) 
    {
    // Print the values of the variables
        cout << endl;
        cout << " _______________________________________________________________"                   << endl;
        cout << "|                                                               |"                  << endl;
        cout << "|" << "\t" << "W  E  L  C  O  M  E    T  O    W  B  P  H  O  N  O  N" << "\t" << "|"   << endl;        
        cout << "|_______________________________________________________________|"                  << endl;
        cout << "|                                                               |"                  << endl;
        if(Nc < 10)
        {
            cout << "|" << "\t" << "Central system\t\t\t\t" << Nc << " atoms" << "\t\t" << "|"           << endl;
        }
        else
        {
            cout << "|" << "\t" << "Central system\t\t\t\t" << Nc << " atoms" << "\t" << "|"           << endl;
        }
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Spring constant:" 
             << "\t\t" << kC << "\t\t" << "|"                                                        << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "On-site energy:"
             << "\t\t\t" << kC0 << "\t\t" << "|"                                                     << endl;
        cout << "|                                                               |"                  << endl;
        cout << "|" << "\t" << "Bath environment\t\t\t\t\t|"                                         << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Spring constant:"
             << "\t\t" << klambda << "\t\t" << "|"                                                   << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "On-site energy:"
             << "\t\t\t" << klambda0 << "\t\t" << "|"                                                << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Left bath inverse temperature:"
             << "\t" << betaL << "\t\t" << "|"                                                       << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Right bath inverse temperature:"
             << "\t" << betaR << "\t\t" << "|"                                                       << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Coupling spring constant:"
             << "\t" << klambdaC << "\t\t" << "|"                                                    << endl;
        cout << "|                                                               |"                  << endl;
        cout << "|" << "\t" << "Transient\t\t\t\t\t\t|"                                              << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Number of timesteps:"
             << "\t\t" << Nt << "\t\t" << "|"                                                        << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Timestep length:"
             << "\t\t" << dt << "\t\t" << "|"                                                        << endl;
        cout << "|                                                               |"                  << endl;
        cout << "|" << "\t" << "Numerical integration\t\t\t\t\t|"                                    << endl;
        cout << "|" << "\t\t" << setprecision(3) << fixed << "Number of subdivisions:"
             << "\t\t" << subs << "\t\t" << "|"                                                      << endl;
        cout << "|" << "\t\t" << setprecision(1) << scientific << "Accuracy:"
             << "\t\t\t" << acc << "\t\t" << "|"                                                     << endl;
        cout << "|_______________________________________________________________|"                  << endl;
        cout << endl;
    }
}
