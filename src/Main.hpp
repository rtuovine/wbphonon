#include <mpi.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <complex>
#include <armadillo>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

using namespace std;
using namespace arma;

void ReadInput(int &systype, int &Nc, int &Nt, int &subs, int &s, int &terminal, double &kC, double &kC0, double &klambda, 
               double &klambda0, double &klambdaC, double &range, double &betaL, double &betaR, 
               double &dt, double &acc, int &sol, int &padeorder);
void ConstructMatrices(int systype, int Nc, int terminal, double kC, double kC0, double klambda, double klambda0, 
                       double klambdaC, cx_mat &Omega0, cx_mat &Omega0Tilde, cx_mat &Omega, 
                       cx_mat &OmegaTilde, cx_mat &OmegaTest, 
                       cx_mat &OmegaTildeTest, cx_mat &LambdaLeft, cx_mat &LambdaRight, 
                       cx_mat &GammaLeft, cx_mat &GammaRight, cx_mat &alpha, cx_mat &OmegaEff, 
                       cx_mat &OmegaEffDagger, cx_vec &Mass);
void DiagonalizeMatrices(int Nc, cx_mat OmegaTilde, cx_mat alpha, cx_mat OmegaEff, 
                         cx_vec &EffEvalL, cx_mat &EffEvecL, cx_vec &EffEvalR, cx_mat &EffEvecR);
void TDCalculation(int Nc, int Nt, int subs, int s, int terminal, double dt, double range, double cutoff, 
                   double betaL, double betaR, double betaC, double klambda, double klambdaC, double acc, 
                   cx_vec EffEvalL, cx_mat EffEvecL, cx_vec EffEvalR, cx_mat EffEvecR, 
                   cx_mat Omega0Tilde, cx_mat OmegaTilde, cx_mat OmegaTildeTest, 
                   cx_mat LambdaLeft, cx_mat LambdaRight, cx_mat GammaLeft, cx_mat GammaRight, 
                   cx_mat alpha, cx_mat OmegaEff, cx_mat OmegaEffDagger, cx_mat &D_lss_ss, 
                   cx_mat &D_lss_ss2, cx_cube &D_lss, cx_mat &jQLeft, cx_mat &jQRight, 
                   cx_vec &jQLeft_ss, cx_vec &jQRight_ss, cx_vec &jQLeft_ss2, cx_vec &jQRight_ss2, 
                   cx_vec &EQ, cx_vec &dEQ, cx_vec Mass, cx_mat &LocalT, cx_mat &dLocalT, 
                   int sol, int padeorder, vec pade_eta, vec pade_zeta,
                   cx_mat &Eloc, cx_mat &dEloc);
void WriteResults(int Nc, int Nt, double dt, cx_cube D_lss, cx_mat D_lss_ss, cx_mat D_lss_ss2, cx_mat jQLeft, 
                  cx_mat jQRight, cx_vec EQ, cx_vec dEQ, cx_mat LocalT, cx_mat dLocalT,
                  cx_mat Eloc, cx_mat dEloc);
cx_mat Bose_mat(double beta, cx_mat A);
double wBose(double beta, double w);
double Sign(double x);
double Theta(double x);
cx_mat LambdaL_w(int Nc, double w, double klambda);
cx_mat LambdaR_w(int Nc, double w, double klambda);
cx_mat GammaL_w(int Nc, double w, double klambda);
cx_mat GammaR_w(int Nc, double w, double klambda);
double Integrand1re(double w, void *param);
double Integrand1im(double w, void *param);
double Integrand2re(double w, void *param);
double Integrand2im(double w, void *param);
double Integrand3re(double w, void *param);
double Integrand3im(double w, void *param);
void StopWatch(double start, double stop);
cx_double ExpIntE(cx_double z);
cx_double ExpExpIntE(cx_double z);
cx_double ExpIntEi(cx_double z);
cx_double ExpExpIntEi(cx_double z);
void PadeExpansion(int padeorder, vec &pade_eta, vec &pade_zeta);
cx_double OneEvaluate(void *param);
cx_double TwoEvaluate(double t, void *param);

struct Params
{
    int j, p, q, Nc;
    double betaL, betaR, dt;
    cx_mat OmegaEff, OmegaEffDagger, tevolj, GammaLeftEff, GammaRightEff;
};

struct Params2
{
    int p, q, Nc, terminal;
    double betaL, betaR, klambda, klambdaC;
    cx_mat alpha, OmegaTilde;
};

struct Params3
{
    int j, k, padeorder;
    double betaL, betaR, cutoff;
    vec pade_eta, pade_zeta;
    cx_vec EffEvalL;
    cx_mat GammaLeftEff_basis, GammaRightEff_basis;
};

template<typename T>    // Test infinity
inline bool isinf(T value)
{
    return std::numeric_limits<T>::has_infinity &&
    value == std::numeric_limits<T>::infinity();
}

template<typename T>    // Test NaN
inline bool isnan(T value)
{
    return value != value;
}
