#include "Main.hpp"
cx_double ExpIntEi(cx_double z)
{
    // Evaluated from E1(z)
    return -ExpIntE(-z) + (log(z) - log(cx_double(1.0,0.0)/z))/2.0 - log(-z);
}

cx_double ExpExpIntEi(cx_double z)
{
    // Evaluated from exp(z)*E1(z)
    return -ExpExpIntE(-z) + (log(z) - log(cx_double(1.0,0.0)/z))/2.0 - log(-z);
}

cx_double ExpIntE(cx_double z)
{
    // Evaluates E1(z)
    cx_double I(0.0,1.0);

    double X, Y, A0, kk;
    cx_double CE1;
    X = real(z);
    Y = imag(z);
    A0 = abs(z);

    if(A0 == 0.0) CE1 = cx_double(1.0e300,0.0);             // Diverges at zero
    else if(A0 <= 10.0 || X < 0.0 && A0 < 20.0)
    {
        CE1 = cx_double(1.0,0.0);
        cx_double CR(1.0,0.0);
        for(int k=1; k<=150; k++)                           // Power series
        {
            kk = static_cast<double>(k);
            CR *= -kk*z/pow((kk+1.0),2);
            CE1 += CR;
            if(abs(CR) <= abs(CE1)*1.0e-15) break;          // Convergence test
        }
        CE1 = cx_double(-math::euler() - log(z) + z*CE1);
    }
    else
    {
        cx_double CT0(0.0,0.0);
        for(int k=120; k>=1; k--)                           // Continued fraction (''bottom up'')
        {
            kk = static_cast<double>(k);
            CT0 = kk/(1.0+kk/(z+CT0));
        }
        cx_double CT(1.0/(z+CT0));

        CE1 = cx_double(exp(-z)*CT);
        if(X <= 0.0 && Y == 0.0) CE1 = CE1-I*math::pi();    // Branch cut on the negative real axis
    }

    return CE1;
}

cx_double ExpExpIntE(cx_double z)
{
    // Evaluates exp(z)*E1(z)
    cx_double I(0.0,1.0);

    double X, Y, A0, kk;
    cx_double CE1;
    X = real(z);
    Y = imag(z);
    A0 = abs(z);

    if(A0 == 0.0) CE1 = cx_double(1.0e300,0.0);             // Diverges at zero
    else if(A0 <= 10.0 || X < 0.0 && A0 < 20.0)
    {
        CE1 = cx_double(1.0,0.0);
        cx_double CR(1.0,0.0);
        for(int k=1; k<=150; k++)                           // Power series
        {
            kk = static_cast<double>(k);
            CR *= -kk*z/pow((kk+1.0),2);
            CE1 += CR;
            if(abs(CR) <= abs(CE1)*1.0e-15) break;          // Convergence test
        }
        CE1 = cx_double(exp(z)*(-math::euler() - log(z) + z*CE1));  // Returns exp(z)*E1(z) instead
    }
    else
    {
        cx_double CT0(0.0,0.0);
        for(int k=120; k>=1; k--)                           // Continued fraction (''bottom up'')
        {
            kk = static_cast<double>(k);
            CT0 = kk/(1.0+kk/(z+CT0));
        }
        cx_double CT(1.0/(z+CT0));

        CE1 = cx_double(CT);                                // Returns exp(z)*E1(z) instead
        if(X <= 0.0 && Y == 0.0) CE1 = CE1-I*math::pi();    // Branch cut on the negative real axis
    }

    return CE1;
}
