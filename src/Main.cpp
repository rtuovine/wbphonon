/*  Last update: May 30th 2016, Riku Tuovinen <riku.m.tuovinen@jyu.fi>  */

#include "Main.hpp"

int main(int argc, char *argv[])
{
// Initialize MPI
    MPI_Init(&argc, &argv);
    double start = MPI_Wtime(); // Start timekeeping
    int size, rank; // Get the number of processes and the individual process ID
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

// Allocate variables and read input
    int systype, Nc, Nt, subs, s, terminal, sol, padeorder;
    double kC, kC0, klambda, klambda0, klambdaC, range, betaL, betaR, dt, acc;
    ReadInput(systype, Nc, Nt, subs, s, terminal, kC, kC0, klambda, klambda0, 
              klambdaC, range, betaL, betaR, dt, acc, sol, padeorder);
    double cutoff = 2.0*sqrt(klambda);  // Cut-off frequency at bandwidth
//    double cutoff = pow(3.0*math::pi()/2.0,(1.0/3.0))*sqrt(klambda);  // Corresponds to a WBA sum rule
    double betaC = 2.0*betaL*betaR/(betaL+betaR); // Central region's temperature in between of L and R
    cx_double I(0.0,1.0);

// Allocate arrays
    vec pade_eta(padeorder), pade_zeta(padeorder);
    cx_vec EffEvalL(2*Nc), EffEvalR(2*Nc), EQ(Nt/size), dEQ(Nt/size), jQLeft_ss(Nc-1), jQRight_ss(Nc-1), 
           jQLeft_ss2(Nc-1), jQRight_ss2(Nc-1), Mass(Nc);
    cx_mat Omega0(2*Nc,2*Nc), Omega0Tilde(2*Nc,2*Nc), Omega(2*Nc,2*Nc), OmegaTilde(2*Nc,2*Nc), 
           OmegaTest(2*Nc,2*Nc), OmegaTildeTest(2*Nc,2*Nc),
           LambdaLeft(2*Nc,2*Nc), LambdaRight(2*Nc,2*Nc), GammaLeft(2*Nc,2*Nc), 
           GammaRight(2*Nc,2*Nc), alpha(2*Nc,2*Nc), OmegaEff(2*Nc,2*Nc), OmegaEffDagger(2*Nc,2*Nc), 
           EffEvecL(2*Nc,2*Nc), EffEvecR(2*Nc,2*Nc), D_lss_ss(2*Nc,2*Nc), D_lss_ss2(2*Nc,2*Nc),
           jQLeft(Nc-1,Nt/size), jQRight(Nc-1,Nt/size), LocalT(Nc,Nt/size), dLocalT(Nc,Nt/size),
           Eloc(Nc,Nt/size), dEloc(Nc,Nt/size);
    cx_cube D_lss(2*Nc,2*Nc,Nt/size);
    Omega0.zeros();
    Omega0Tilde.zeros();
    Omega.zeros();
    OmegaTilde.zeros();
    OmegaTest.zeros();
    OmegaTildeTest.zeros();
    LambdaLeft.zeros();
    LambdaRight.zeros();
    GammaLeft.zeros();
    GammaRight.zeros();
    alpha.zeros();
    OmegaEff.zeros();
    OmegaEffDagger.zeros();
    D_lss_ss.zeros();
    D_lss_ss2.zeros();
    D_lss.zeros();
    EffEvalL.zeros();
    EffEvecL.zeros();
    EffEvalR.zeros();
    EffEvecR.zeros();
    jQLeft.zeros();
    jQRight.zeros();
    EQ.zeros();
    dEQ.zeros();
    jQLeft_ss.zeros();
    jQRight_ss.zeros();
    jQLeft_ss2.zeros();
    jQRight_ss2.zeros();
    Mass.zeros();
    LocalT.zeros();
    dLocalT.zeros();
    Eloc.zeros();
    dEloc.zeros();

// Construct matrices
    ConstructMatrices(systype, Nc, terminal, kC, kC0, klambda, klambda0, klambdaC, Omega0, Omega0Tilde, 
                      Omega, OmegaTilde, OmegaTest, OmegaTildeTest, LambdaLeft, LambdaRight, GammaLeft, GammaRight, alpha, 
                      OmegaEff, OmegaEffDagger, Mass);

// Diagonalize matrices
    DiagonalizeMatrices(Nc, OmegaTilde, alpha, OmegaEff, EffEvalL, EffEvecL, 
                        EffEvalR, EffEvecR);

// Time-dependent calculation
    TDCalculation(Nc, Nt, subs, s, terminal, dt, range, cutoff, betaL, 
                  betaR, betaC, klambda, klambdaC, acc, EffEvalL, EffEvecL, 
                  EffEvalR, EffEvecR, Omega0Tilde, OmegaTilde, OmegaTildeTest, 
                  LambdaLeft, LambdaRight, GammaLeft, GammaRight, alpha, OmegaEff, 
                  OmegaEffDagger, D_lss_ss, D_lss_ss2, D_lss, jQLeft, jQRight, 
                  jQLeft_ss, jQRight_ss, jQLeft_ss2, jQRight_ss2, EQ, dEQ, 
                  Mass, LocalT, dLocalT, sol, padeorder, pade_eta, pade_zeta,
                  Eloc, dEloc);

// Diagonalize the density matrix (testing)
/*
    field<cx_vec> EigFreqs(Nt/size);
    field<cx_mat> EigModes(Nt/size);
    cout << "Diagonalization" << endl;
    for(int i=0; i<Nt/size; i++)
    {
        cout << i << endl;
        eig_gen(EigFreqs(i), EigModes(i), D_lss.slice(i));
    }

    EigFreqs(0).print();
*/

// Print and write results
    cx_mat tmp(2*Nc,2*Nc);
    tmp.zeros();
    tmp = 0.5*(D_lss_ss2 + trans(D_lss_ss2));
    if(rank == 0 && Nc <= 4)
    {
        D_lss.slice(0).print("\nInitial density matrix = ");
        D_lss_ss.print("\nSteady-state density matrix (WBA) = ");
        cout << endl;
        cout << "Canonical commutation relation (WBA):" << endl;
        for(unsigned int j=0; j<Nc; j++)
        {
            cout << D_lss_ss(j+Nc,j) - D_lss_ss(j,Nc+j) << " ";
        }
        cout << endl;
        if(sol==1)
        {
            D_lss_ss2.print("\nSteady-state density matrix (Exact) = ");
            cout << endl;
            cout << "Canonical commutation relation (Exact):" << endl;
            for(unsigned int j=0; j<Nc; j++)
            {
                cout << D_lss_ss2(j+Nc,j) - D_lss_ss2(j,Nc+j) << " ";
            }
            cout << endl;
            cout << endl;
            tmp.print("\nSteady-state density matrix (TEST) = ");
            cout << endl;
            cout << "Canonical commutation relation (TEST):" << endl;
            for(unsigned int j=0; j<Nc; j++)
            {
                cout << tmp(j+Nc,j) - tmp(j,Nc+j) << " ";
            }
            cout << endl;

        }
    }
    if(rank == size-1)
    {
        cout << endl; cout << "Saving files and terminating... " << endl;
        cout << endl;
    }
    WriteResults(Nc, Nt, dt, D_lss, D_lss_ss, D_lss_ss2, jQLeft, jQRight, EQ, dEQ, LocalT, dLocalT, Eloc, dEloc);

// Terminate MPI
    double finish = MPI_Wtime();    // Stop timekeeping
    if(rank == 0) StopWatch(start, finish); // Stopwatch
    MPI_Finalize();
    return 0;
}
