#include "Main.hpp"
void ConstructMatrices(int systype, int Nc, int terminal, double kC, double kC0, double klambda, double klambda0, 
                       double klambdaC, cx_mat &Omega0, cx_mat &Omega0Tilde, cx_mat &Omega, 
                       cx_mat &OmegaTilde, cx_mat &OmegaTest, 
                       cx_mat &OmegaTildeTest, cx_mat &LambdaLeft, cx_mat &LambdaRight, cx_mat &GammaLeft, 
                       cx_mat &GammaRight, cx_mat &alpha, cx_mat &OmegaEff, cx_mat &OmegaEffDagger, cx_vec &Mass)
{
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    cx_double I(0.0,1.0);

    if(systype == 1)
    {
        if(terminal == 0)
        {
            for(int j=0; j<2*Nc; j++)
            {
                if(j < Nc) Mass(j) = 1.0;
                for(int k=0; k<2*Nc; k++)
                {
                    if(j < Nc && k < Nc)
                    {
     	         	    if(j == k)
                        {
                            if(j==0 || j==Nc-1)
                            {
//                                Omega(j,k) = klambdaC + kC0;
//                                OmegaTest(j,k) = kC + kC0;
                                Omega(j,k) = 2.0*kC + kC0;
                                OmegaTest(j,k) = 2.0*kC + kC0;
                            }
                            else 
                            {
                                Omega(j,k) = 2.0*kC + kC0;
                                OmegaTest(j,k) = 2.0*kC + kC0;
                            }
                        }
                        else if(abs(j-k) == 1)
                        {
                            Omega(j,k) = -kC;
                            OmegaTest(j,k) = -kC;
                        }
                    }
                    else if(j == k)
                    {
                        Omega(j,k) = 1.0;
                        OmegaTest(j,k) = 1.0;
                    }

                    if(Nc == 1)
                    {
                        if(j == 0 && k == 0) 
                        {
                            LambdaLeft(j,k) = -pow(klambdaC,2)/klambda;
                            GammaLeft(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
                            LambdaRight(j,k) = -pow(klambdaC,2)/klambda;
                            GammaRight(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
		                }
		            }
		            else
		            {
                        if(j == 0 && k == 0) 
		                {
                            LambdaLeft(j,k) = -pow(klambdaC,2)/klambda;
			                GammaLeft(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
		                }
		                else if(j == Nc-1 && k == Nc-1) 
		                {
		                    LambdaRight(j,k) = -pow(klambdaC,2)/klambda;
			                GammaRight(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
		                }
		            }

        		    if(j >= Nc && k <= Nc)
		            {
                        if(j >= k + Nc && j <= k + Nc) alpha(j,k) = -I;
		            }
		            else if(j <= Nc && k >= Nc)
		            {
                        if(k >= j + Nc && k <= j + Nc) alpha(j,k) = I;
		            }
	            }
    	    }
        }
        else if(terminal == 1)
        {
            for(int j=0; j<2*Nc; j++)
	        {
	            if(j < Nc) Mass(j) = 1.0;
                for(int k=0; k<2*Nc; k++)
	            {
	                if(j < Nc && k < Nc)
		            {
                        if(j == k) Omega(j,k) = 2.0*kC + kC0;
		                else if(abs(j-k) == 1) Omega(j,k) = -kC;
		            }
		            else if(j == k) Omega(j,k) = 1.0;

		            if(Nc == 1)
		            {
                        if(j == 0 && k == 0) 
		                {
                            LambdaLeft(j,k) = -pow(klambdaC,2)/klambda;
			                GammaLeft(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
			                LambdaRight(j,k) = -pow(klambdaC,2)/klambda;
			                GammaRight(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
		                }
		            }
		            else
		            {
                        if(j == 0 && k == 0) 
		                {
                            LambdaLeft(j,k) = -pow(klambdaC,2)/klambda;
			                GammaLeft(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
		                }
		                else if(j == Nc-1 && k == Nc-1) 
		                {
                            LambdaRight(j,k) = -pow(klambdaC,2)/klambda;
			                GammaRight(j,k) = 2.0*pow(klambdaC,2)/pow(klambda,1.5);
		                }
		            }

		            if(j >= Nc && k <= Nc)
		            {
                        if(j >= k + Nc && j <= k + Nc) alpha(j,k) = -I;
		            }
		            else if(j <= Nc && k >= Nc)
                    {
                        if(k >= j + Nc && k <= j + Nc) alpha(j,k) = I;
		            }
	            }
	        }
        }
        else {cout << "Erroneous terminal parameter in ConstructMatrices.cpp, exiting ..." << endl; exit(1); }
    }

    else if(systype == 2)   // Hard-coded matrix structures
    {
        double gL = 1.0;
        double gR = 1.0;
        double gC = 1.0;
        double gLC = 0.1;
        double gRC = 0.1;
        double g2 = 1.0;
        double g3 = 1.0;
        double m1 = 1.0;
        double m2 = 1.0;
        double m3 = 1.5;
        double m4 = 1.0;

        Omega(0,0)=(gL+gLC)/sqrt(m1*m1); Omega(0,1)=-gLC/sqrt(m1*m2);        Omega(0,2)=0.0;                     Omega(0,3)=0.0;                  Omega(0,4)=0.0; Omega(0,5)=0.0; Omega(0,6)=0.0; Omega(0,7)=0.0;
        Omega(1,0)=-gLC/sqrt(m2*m1);     Omega(1,1)=(gLC+gC+g2)/sqrt(m2*m2); Omega(1,2)=-gC/sqrt(m2*m3);         Omega(1,3)=0.0;                  Omega(1,4)=0.0; Omega(1,5)=0.0; Omega(1,6)=0.0; Omega(1,7)=0.0;
        Omega(2,0)=0.0;                  Omega(2,1)=-gC/sqrt(m3*m2);         Omega(2,2)=(gC+gRC+g3)/sqrt(m3*m3); Omega(2,3)=-gRC/sqrt(m3*m4);     Omega(2,4)=0.0; Omega(2,5)=0.0; Omega(2,6)=0.0; Omega(2,7)=0.0;
        Omega(3,0)=0.0;                  Omega(3,1)=0.0;                     Omega(3,2)=-gRC/sqrt(m4*m3);        Omega(3,3)=(gR+gRC)/sqrt(m4*m4); Omega(3,4)=0.0; Omega(3,5)=0.0; Omega(3,6)=0.0; Omega(3,7)=0.0;
        Omega(4,0)=0.0;                  Omega(4,1)=0.0;                     Omega(4,2)=0.0;                     Omega(4,3)=0.0;                  Omega(4,4)=1.0; Omega(4,5)=0.0; Omega(4,6)=0.0; Omega(4,7)=0.0;
        Omega(5,0)=0.0;                  Omega(5,1)=0.0;                     Omega(5,2)=0.0;                     Omega(5,3)=0.0;                  Omega(5,4)=0.0; Omega(5,5)=1.0; Omega(5,6)=0.0; Omega(5,7)=0.0;
        Omega(6,0)=0.0;                  Omega(6,1)=0.0;                     Omega(6,2)=0.0;                     Omega(6,3)=0.0;                  Omega(6,4)=0.0; Omega(6,5)=0.0; Omega(6,6)=1.0; Omega(6,7)=0.0;
        Omega(7,0)=0.0;                  Omega(7,1)=0.0;                     Omega(7,2)=0.0;                     Omega(7,3)=0.0;                  Omega(7,4)=0.0; Omega(7,5)=0.0; Omega(7,6)=0.0; Omega(7,7)=1.0;

        LambdaLeft(0,0) = -gL;
        LambdaRight(3,3) = -gR;
        GammaLeft(0,0) = 2.0*sqrt(gL);
        GammaRight(3,3) = 2.0*sqrt(gR);
    }

    else if(systype == 3)   //From file
    {
        ifstream inputOmega0("input/Omega0.in");
        if(!inputOmega0){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aOmega0, bOmega0;
        double cOmega0, dOmega0;

        string lineOmega0;
        while(getline(inputOmega0, lineOmega0))
        {
            stringstream ssOmega0(lineOmega0);
            if (ssOmega0 >> aOmega0 >> bOmega0 >> cOmega0 >> dOmega0)
            {
                Omega0(aOmega0,bOmega0) = cOmega0 + I*dOmega0;
            }
        }

        ifstream inputOmega("input/Omega.in");
        if(!inputOmega){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aOmega, bOmega;
        double cOmega, dOmega;

        string lineOmega;
        while(getline(inputOmega, lineOmega))
        {
            stringstream ssOmega(lineOmega);
            if (ssOmega >> aOmega >> bOmega >> cOmega >> dOmega)
            {
                Omega(aOmega,bOmega) = cOmega + I*dOmega;
            }
        }

        ifstream inputalpha("input/alpha.in");
        if(!inputalpha){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aalpha, balpha;
        double calpha, dalpha;

        string linealpha;
        while(getline(inputalpha, linealpha))
        {
            stringstream ssalpha(linealpha);
            if (ssalpha >> aalpha >> balpha >> calpha >> dalpha)
            {
                alpha(aalpha,balpha) = calpha + I*dalpha;
            }
        }

        ifstream inputLambdaLeft("input/LambdaLeft.in");
        if(!inputLambdaLeft){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aLambdaLeft, bLambdaLeft;
        double cLambdaLeft, dLambdaLeft;

        string lineLambdaLeft;
        while(getline(inputLambdaLeft, lineLambdaLeft))
        {
            stringstream ssLambdaLeft(lineLambdaLeft);
            if (ssLambdaLeft >> aLambdaLeft >> bLambdaLeft >> cLambdaLeft >> dLambdaLeft)
            {
                LambdaLeft(aLambdaLeft,bLambdaLeft) = cLambdaLeft + I*dLambdaLeft;
            }
        }

        ifstream inputLambdaRight("input/LambdaRight.in");
        if(!inputLambdaRight){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aLambdaRight, bLambdaRight;
        double cLambdaRight, dLambdaRight;

        string lineLambdaRight;
        while(getline(inputLambdaRight, lineLambdaRight))
        {
            stringstream ssLambdaRight(lineLambdaRight);
            if (ssLambdaRight >> aLambdaRight >> bLambdaRight >> cLambdaRight >> dLambdaRight)
            {
                LambdaRight(aLambdaRight,bLambdaRight) = cLambdaRight + I*dLambdaRight;
            }
        }

        ifstream inputGammaLeft("input/GammaLeft.in");
        if(!inputGammaLeft){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aGammaLeft, bGammaLeft;
        double cGammaLeft, dGammaLeft;

        string lineGammaLeft;
        while(getline(inputGammaLeft, lineGammaLeft))
        {
            stringstream ssGammaLeft(lineGammaLeft);
            if (ssGammaLeft >> aGammaLeft >> bGammaLeft >> cGammaLeft >> dGammaLeft)
            {
                GammaLeft(aGammaLeft,bGammaLeft) = cGammaLeft + I*dGammaLeft;
            }
        }

        ifstream inputGammaRight("input/GammaRight.in");
        if(!inputGammaRight){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aGammaRight, bGammaRight;
        double cGammaRight, dGammaRight;

        string lineGammaRight;
        while(getline(inputGammaRight, lineGammaRight))
        {
            stringstream ssGammaRight(lineGammaRight);
            if (ssGammaRight >> aGammaRight >> bGammaRight >> cGammaRight >> dGammaRight)
            {
                GammaRight(aGammaRight,bGammaRight) = cGammaRight + I*dGammaRight;
            }
        }

        ifstream inputMass("input/Mass.in");
        if(!inputMass){ cout << "Erroneous input file, exiting ..." << endl; exit(1);}

        unsigned int aMass;
        double cMass;

        string lineMass;
        while(getline(inputMass, lineMass))
        {
            stringstream ssMass(lineMass);
            if (ssMass >> aMass >> cMass)
            {
                Mass(aMass) = cMass;
            }
        }
    }

    else {cout << "Erroneous system parameter in ConstructMatrices.cpp, exiting ..." << endl; exit(1);}

    Omega0 *= 0.5;
    Omega0Tilde = Omega0 + strans(Omega0);

    Omega *= 0.5;
    OmegaTilde = Omega + strans(Omega);

    OmegaTest *= 0.5;
    OmegaTildeTest = OmegaTest + strans(OmegaTest);

    OmegaEff = inv(alpha + 0.5*I*(GammaLeft + GammaRight))*(OmegaTilde + LambdaLeft + LambdaRight);
    OmegaEffDagger = trans(OmegaEff);
//    OmegaEffDagger = (OmegaTilde + LambdaLeft + LambdaRight)*inv(alpha - 0.5*I*(GammaLeft + GammaRight));

    mat test;

    if(rank == 0 && Nc <= 4)
    {
        test = real(Omega0);
        test.print("\nOmega0 = ");
        test = real(Omega);
        test.print("\nOmega = ");
        test = real(LambdaLeft);
        test.print("\nLambdaLeft = ");
        test = real(LambdaRight);
        test.print("\nLambdaRight = ");
        test = real(GammaLeft);
        test.print("\nGammaLeft = ");
        test = real(GammaRight);
        test.print("\nGammaRight = ");
        test = imag(alpha);
        test.print("\nIm[alpha] = ");
        test = imag(OmegaEff);
        test.print("\nIm[OmegaEff] = ");
    }
}
