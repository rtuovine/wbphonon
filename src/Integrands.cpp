#include "Main.hpp"
double Integrand1re(double w, void *param)
{
    Params &funcpara= *reinterpret_cast<Params *>(param);

    int j = funcpara.j;
    int p = funcpara.p;
    int q = funcpara.q;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    cx_mat OmegaEff = funcpara.OmegaEff;
    cx_mat OmegaEffDagger = funcpara.OmegaEffDagger;
    cx_mat tevolj = funcpara.tevolj;
    cx_mat GammaLeftEff = funcpara.GammaLeftEff;
    cx_mat GammaRightEff = funcpara.GammaRightEff;
    int Nc = funcpara.Nc;
    double dt = funcpara.dt;

    cx_double I(0.0,1.0);
    cx_double jj(static_cast<double>(j),0.0);

    cx_mat product(2*Nc,2*Nc);

    product = wBose(betaL,w)*(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj)
             *inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff
             *trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj)
            + wBose(betaR,w)*(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj)
             *inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff
             *trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj);

/*
    product = wBose(betaL,w)
             *( inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               -inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj)
               -exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               +exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj) 
               )

            + wBose(betaR,w)
             *( inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               -inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj)
               -exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               +exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj) 
               );
*/

    // w*(cosh(betaL*w/2.0)/sinh(betaL*w/2.0))

    return real(product(p,q));
}

double Integrand1im(double w, void *param)
{
    Params &funcpara= *reinterpret_cast<Params *>(param);

    int j = funcpara.j;
    int p = funcpara.p;
    int q = funcpara.q;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    cx_mat OmegaEff = funcpara.OmegaEff;
    cx_mat OmegaEffDagger = funcpara.OmegaEffDagger;
    cx_mat tevolj = funcpara.tevolj;
    cx_mat GammaLeftEff = funcpara.GammaLeftEff;
    cx_mat GammaRightEff = funcpara.GammaRightEff;
    int Nc = funcpara.Nc;
    double dt = funcpara.dt;

    cx_double I(0.0,1.0);
    cx_double jj(static_cast<double>(j),0.0);

    cx_mat product(2*Nc,2*Nc);

    product = wBose(betaL,w)*(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj)
             *inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff
             *trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj)
            + wBose(betaR,w)*(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj)
             *inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff
             *trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(eye(2*Nc,2*Nc) - exp(I*w*jj*dt)*tevolj);
/*

    product = wBose(betaL,w)
             *( inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               -inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj)
               -exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               +exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj) 
               )

            + wBose(betaR,w)
             *( inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               -inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj)
               -exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
               +exp(I*w*jj*dt)*tevolj*inv(w*eye(2*Nc,2*Nc) - OmegaEff)*GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))*trans(exp(I*w*jj*dt)*tevolj) 
               );
*/

//  w*(cosh(betaL*w/2.0)/sinh(betaL*w/2.0))

    return imag(product(p,q));
}

double Integrand2re(double w, void *param)
{
    Params &funcpara= *reinterpret_cast<Params *>(param);

    int j = funcpara.j;
    int p = funcpara.p;
    int q = funcpara.q;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    cx_mat OmegaEff = funcpara.OmegaEff;
    cx_mat OmegaEffDagger = funcpara.OmegaEffDagger;
    cx_mat tevolj = funcpara.tevolj;
    cx_mat GammaLeftEff = funcpara.GammaLeftEff;
    cx_mat GammaRightEff = funcpara.GammaRightEff;
    int Nc = funcpara.Nc;
    double dt = funcpara.dt;

    cx_double I(0.0,1.0);

    cx_mat product(2*Nc,2*Nc);

    product = wBose(betaL,w)*inv(w*eye(2*Nc,2*Nc) - OmegaEff)
             *GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
            + wBose(betaR,w)*inv(w*eye(2*Nc,2*Nc) - OmegaEff)
             *GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff));

    ofstream spectral("output/spectral.out", ios::app);
    spectral << setprecision(5) << scientific << w << "\t" << real(product(0,0)) << endl;
    spectral.close();

    return real(product(p,q));
}

double Integrand2im(double w, void *param)
{
    Params &funcpara= *reinterpret_cast<Params *>(param);

    int j = funcpara.j;
    int p = funcpara.p;
    int q = funcpara.q;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    cx_mat OmegaEff = funcpara.OmegaEff;
    cx_mat OmegaEffDagger = funcpara.OmegaEffDagger;
    cx_mat tevolj = funcpara.tevolj;
    cx_mat GammaLeftEff = funcpara.GammaLeftEff;
    cx_mat GammaRightEff = funcpara.GammaRightEff;
    int Nc = funcpara.Nc;
    double dt = funcpara.dt;

    cx_double I(0.0,1.0);

    cx_mat product(2*Nc,2*Nc);

    product = wBose(betaL,w)*inv(w*eye(2*Nc,2*Nc) - OmegaEff)
             *GammaLeftEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff))
            + wBose(betaR,w)*inv(w*eye(2*Nc,2*Nc) - OmegaEff)
             *GammaRightEff*trans(inv(w*eye(2*Nc,2*Nc) - OmegaEff));

    return imag(product(p,q));
}

double Integrand3re(double w, void *param)
{
    Params2 &funcpara= *reinterpret_cast<Params2 *>(param);

    int p = funcpara.p;
    int q = funcpara.q;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    double klambda = funcpara.klambda;
    cx_mat alpha = funcpara.alpha;
    cx_mat OmegaTilde = funcpara.OmegaTilde;
    int Nc = funcpara.Nc;
    int terminal = funcpara.terminal;
    double klambdaC = funcpara.klambdaC;

    cx_double I(0.0,1.0);

    cx_mat product(2*Nc,2*Nc);

    cx_mat LambdaLeft(2*Nc,2*Nc), LambdaRight(2*Nc,2*Nc), GammaLeft(2*Nc,2*Nc), GammaRight(2*Nc,2*Nc);
    LambdaLeft.zeros();
    LambdaRight.zeros();
    GammaLeft.zeros();
    GammaRight.zeros();
    if(terminal == 0)
    {
        LambdaLeft(0,0) = -((klambdaC*klambdaC)/(2.0*klambda*klambda))*(2.0*klambda - w*w);
        LambdaRight(Nc-1,Nc-1) = -((klambdaC*klambdaC)/(2.0*klambda*klambda))*(2.0*klambda - w*w);
        GammaLeft(0,0) = ((klambdaC*klambdaC)/(klambda*klambda))*sqrt(abs(w*w - 4.0*klambda));
        GammaRight(Nc-1,Nc-1) = ((klambdaC*klambdaC)/(klambda*klambda))*sqrt(abs(w*w - 4.0*klambda));
    }
    else if (terminal == 1)
    {
        LambdaLeft(0,0) = -(klambda - 0.5*w*w);
        LambdaRight(Nc-1,Nc-1) = -(klambda - 0.5*w*w);
        GammaLeft(0,0) = sqrt(abs(w*w - 4.0*klambda));
        GammaRight(Nc-1,Nc-1) = sqrt(abs(w*w - 4.0*klambda));
    }
    else {cout << "Erroneous terminal parameter in Integrands.cpp, exiting ..." << endl; exit(1); }

    ofstream lambdal("output/LambdaL.out", ios::app);
    lambdal << setprecision(5) << scientific << w << "\t" << real(LambdaLeft(0,0)) << endl;
    lambdal.close();
    ofstream lambdar("output/LambdaR.out", ios::app);
    lambdar << setprecision(5) << scientific << w << "\t" << real(LambdaRight(Nc-1,Nc-1)) << endl;
    lambdar.close();
    ofstream gammal("output/GammaL.out", ios::app);
    gammal << setprecision(5) << scientific << w << "\t" << real(w*GammaLeft(0,0)) << endl;
    gammal.close();
    ofstream gammar("output/GammaR.out", ios::app);
    gammar << setprecision(5) << scientific << w << "\t" << real(w*GammaRight(Nc-1,Nc-1)) << endl;
    gammar.close();

    product = inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha*wBose(betaL,w)*GammaLeft
             *trans(inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha)
             +inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha*wBose(betaR,w)*GammaRight
             *trans(inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha);

    return real(product(p,q));
}

double Integrand3im(double w, void *param)
{
    Params2 &funcpara= *reinterpret_cast<Params2 *>(param);

    int p = funcpara.p;
    int q = funcpara.q;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    double klambda = funcpara.klambda;
    cx_mat alpha = funcpara.alpha;
    cx_mat OmegaTilde = funcpara.OmegaTilde;
    int Nc = funcpara.Nc;
    int terminal = funcpara.terminal;
    double klambdaC = funcpara.klambdaC;

    cx_double I(0.0,1.0);

    cx_mat product(2*Nc,2*Nc);

    cx_mat LambdaLeft(2*Nc,2*Nc), LambdaRight(2*Nc,2*Nc), GammaLeft(2*Nc,2*Nc), GammaRight(2*Nc,2*Nc);
    LambdaLeft.zeros();
    LambdaRight.zeros();
    GammaLeft.zeros();
    GammaRight.zeros();
    if(terminal == 0)
    {
        LambdaLeft(0,0) = -((klambdaC*klambdaC)/(2.0*klambda*klambda))*(2.0*klambda - w*w);
        LambdaRight(Nc-1,Nc-1) = -((klambdaC*klambdaC)/(2.0*klambda*klambda))*(2.0*klambda - w*w);
        GammaLeft(0,0) = ((klambdaC*klambdaC)/(klambda*klambda))*sqrt(abs(w*w - 4.0*klambda));
        GammaRight(Nc-1,Nc-1) = ((klambdaC*klambdaC)/(klambda*klambda))*sqrt(abs(w*w - 4.0*klambda));
    }
    else if (terminal == 1)
    {
        LambdaLeft(0,0) = -(klambda - 0.5*w*w);
        LambdaRight(Nc-1,Nc-1) = -(klambda - 0.5*w*w);
        GammaLeft(0,0) = sqrt(abs(w*w - 4.0*klambda));
        GammaRight(Nc-1,Nc-1) = sqrt(abs(w*w - 4.0*klambda));
    }
    else {cout << "Erroneous terminal parameter in Integrands.cpp, exiting ..." << endl; exit(1); }

    product = inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha*wBose(betaL,w)*GammaLeft
             *trans(inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha)
             +inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha*wBose(betaR,w)*GammaRight
             *trans(inv(w*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*w*(GammaLeft + GammaRight))*alpha);

    return imag(product(p,q));
}

cx_double OneEvaluate(void *param)
{
    Params3 &funcpara= *reinterpret_cast<Params3 *>(param);

    int j = funcpara.j;
    int k = funcpara.k;
    int padeorder = funcpara.padeorder;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    double cutoff = funcpara.cutoff;
    vec pade_eta = funcpara.pade_eta;
    vec pade_zeta = funcpara.pade_zeta;
    cx_vec EffEvalL = funcpara.EffEvalL;
    cx_mat GammaLeftEff_basis = funcpara.GammaLeftEff_basis;
    cx_mat GammaRightEff_basis = funcpara.GammaRightEff_basis;

    cx_double I(0.0,1.0);

    cx_double tempsum(0.0,0.0);

    for(unsigned int l=0; l<padeorder; l++)
    {
        tempsum += (pade_eta(l)/(2.0*math::pi()*betaL))*GammaLeftEff_basis(j,k)
                  *(cx_double(1.0,0.0)/(EffEvalL(j) - conj(EffEvalL(k))))
                  *(

                    (cx_double(1.0,0.0)/(EffEvalL(j) + I*pade_zeta(l)/betaL))
                   *(EffEvalL(j)*log(cx_double(cutoff,0.0) - EffEvalL(j))
                    -EffEvalL(j)*log(cx_double(-cutoff,0.0) - EffEvalL(j))
                    +(I*pade_zeta(l)/betaL)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaL)
                    -(I*pade_zeta(l)/betaL)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaL))

                   +(cx_double(1.0,0.0)/(EffEvalL(j) - I*pade_zeta(l)/betaL))
                   *(EffEvalL(j)*log(cx_double(cutoff,0.0) - EffEvalL(j))
                    -EffEvalL(j)*log(cx_double(-cutoff,0.0) - EffEvalL(j))
                    +(I*pade_zeta(l)/betaL)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaL)
                    -(I*pade_zeta(l)/betaL)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaL))

                   +(cx_double(1.0,0.0)/(conj(EffEvalL(k)) + I*pade_zeta(l)/betaL))
                   *(conj(EffEvalL(k))*log(cx_double(cutoff,0.0) + conj(EffEvalL(k)))
                    -conj(EffEvalL(k))*log(cx_double(-cutoff,0.0) + conj(EffEvalL(k)))
                    -(I*pade_zeta(l)/betaL)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaL)
                    +(I*pade_zeta(l)/betaL)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaL))

                   +(cx_double(1.0,0.0)/(conj(EffEvalL(k)) - I*pade_zeta(l)/betaL))
                   *(conj(EffEvalL(k))*log(cx_double(cutoff,0.0) + conj(EffEvalL(k)))
                    -conj(EffEvalL(k))*log(cx_double(-cutoff,0.0) + conj(EffEvalL(k)))
                    -(I*pade_zeta(l)/betaL)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaL)
                    +(I*pade_zeta(l)/betaL)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaL))

                    )

                  +(pade_eta(l)/(2.0*math::pi()*betaR))*GammaRightEff_basis(j,k)
                  *(cx_double(1.0,0.0)/(EffEvalL(j) - conj(EffEvalL(k))))
                  *(

                    (cx_double(1.0,0.0)/(EffEvalL(j) + I*pade_zeta(l)/betaR))
                   *(EffEvalL(j)*log(cx_double(cutoff,0.0) - EffEvalL(j))
                    -EffEvalL(j)*log(cx_double(-cutoff,0.0) - EffEvalL(j))
                    +(I*pade_zeta(l)/betaR)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaR)
                    -(I*pade_zeta(l)/betaR)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaR))

                   +(cx_double(1.0,0.0)/(EffEvalL(j) - I*pade_zeta(l)/betaR))
                   *(EffEvalL(j)*log(cx_double(cutoff,0.0) - EffEvalL(j))
                    -EffEvalL(j)*log(cx_double(-cutoff,0.0) - EffEvalL(j))
                    +(I*pade_zeta(l)/betaR)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaR)
                    -(I*pade_zeta(l)/betaR)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaR))

                   +(cx_double(1.0,0.0)/(conj(EffEvalL(k)) + I*pade_zeta(l)/betaR))
                   *(conj(EffEvalL(k))*log(cx_double(cutoff,0.0) + conj(EffEvalL(k)))
                    -conj(EffEvalL(k))*log(cx_double(-cutoff,0.0) + conj(EffEvalL(k)))
                    -(I*pade_zeta(l)/betaR)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaR)
                    +(I*pade_zeta(l)/betaR)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaR))

                   +(cx_double(1.0,0.0)/(conj(EffEvalL(k)) - I*pade_zeta(l)/betaR))
                   *(conj(EffEvalL(k))*log(cx_double(cutoff,0.0) + conj(EffEvalL(k)))
                    -conj(EffEvalL(k))*log(cx_double(-cutoff,0.0) + conj(EffEvalL(k)))
                    -(I*pade_zeta(l)/betaR)*log(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaR)
                    +(I*pade_zeta(l)/betaR)*log(cx_double(-cutoff,0.0) + I*pade_zeta(l)/betaR))

                    );
    }

    return ((1.0/(2.0*math::pi()*betaL))*GammaLeftEff_basis(j,k)
          *(cx_double(1.0,0.0)/(EffEvalL(j) - conj(EffEvalL(k))))
          *(log(cx_double(cutoff,0.0) - EffEvalL(j)) - log(cx_double(-cutoff,0.0) - EffEvalL(j))
           +log(cx_double(cutoff,0.0) + conj(EffEvalL(k))) - log(cx_double(-cutoff,0.0) + conj(EffEvalL(k))))
           
          -(1.0/(4.0*math::pi()))*GammaLeftEff_basis(j,k)
          *(cx_double(1.0,0.0)/(EffEvalL(j) - conj(EffEvalL(k))))
          *(EffEvalL(j)*log(cx_double(cutoff,0.0) - EffEvalL(j)) - EffEvalL(j)*log(cx_double(-cutoff,0.0) - EffEvalL(j))
           +conj(EffEvalL(k))*log(cx_double(cutoff,0.0) + conj(EffEvalL(k))) - conj(EffEvalL(k))*log(cx_double(-cutoff,0.0) + conj(EffEvalL(k))))

          +(1.0/(2.0*math::pi()*betaR))*GammaRightEff_basis(j,k)
          *(cx_double(1.0,0.0)/(EffEvalL(j) - conj(EffEvalL(k))))
          *(log(cx_double(cutoff,0.0) - EffEvalL(j)) - log(cx_double(-cutoff,0.0) - EffEvalL(j))
           +log(cx_double(cutoff,0.0) + conj(EffEvalL(k))) - log(cx_double(-cutoff,0.0) + conj(EffEvalL(k))))
           
          -(1.0/(4.0*math::pi()))*GammaRightEff_basis(j,k)
          *(cx_double(1.0,0.0)/(EffEvalL(j) - conj(EffEvalL(k))))
          *(EffEvalL(j)*log(cx_double(cutoff,0.0) - EffEvalL(j)) - EffEvalL(j)*log(cx_double(-cutoff,0.0) - EffEvalL(j))
           +conj(EffEvalL(k))*log(cx_double(cutoff,0.0) + conj(EffEvalL(k))) - conj(EffEvalL(k))*log(cx_double(-cutoff,0.0) + conj(EffEvalL(k))))

          +tempsum

           );
}

cx_double TwoEvaluate(double t, void *param)
{
    Params3 &funcpara= *reinterpret_cast<Params3 *>(param);

    int j = funcpara.j;
    int k = funcpara.k;
    int padeorder = funcpara.padeorder;
    double betaL = funcpara.betaL;
    double betaR = funcpara.betaR;
    double cutoff = funcpara.cutoff;
    vec pade_eta = funcpara.pade_eta;
    vec pade_zeta = funcpara.pade_zeta;
    cx_vec EffEvalL = funcpara.EffEvalL;
    cx_mat GammaLeftEff_basis = funcpara.GammaLeftEff_basis;
    cx_mat GammaRightEff_basis = funcpara.GammaRightEff_basis;

    cx_double I(0.0,1.0);

    cx_double tempsum(0.0,0.0);

    for(unsigned int l=0; l<padeorder; l++)
    {
        tempsum += (pade_eta(l)/betaL)*GammaLeftEff_basis(j,k)
                  *(

                    (cx_double(1.0,0.0)/(EffEvalL(j) + I*pade_zeta(l)/betaL))
                   *(EffEvalL(j)*ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
                    -EffEvalL(j)*ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
                    -EffEvalL(j)*2.0*math::pi()*I
                    +(I*pade_zeta(l)/betaL)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntE(I*(cx_double(cutoff,0.0)-I*pade_zeta(l)/betaL)*t)
                    -(I*pade_zeta(l)/betaL)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntE(-I*(cx_double(cutoff,0.0)+I*pade_zeta(l)/betaL)*t))

                   -(cx_double(1.0,0.0)/(conj(EffEvalL(k)) + I*pade_zeta(l)/betaL))
                   *(conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t)
                    -conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t)
                    +(I*pade_zeta(l)/betaL)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntE(I*(cx_double(cutoff,0.0)-I*pade_zeta(l)/betaL)*t)
                    -(I*pade_zeta(l)/betaL)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntE(-I*(cx_double(cutoff,0.0)+I*pade_zeta(l)/betaL)*t))

                   +(cx_double(1.0,0.0)/(EffEvalL(j) - I*pade_zeta(l)/betaL))
                   *(EffEvalL(j)*ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
                    -EffEvalL(j)*ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
                    -EffEvalL(j)*2.0*math::pi()*I
                    +(I*pade_zeta(l)/betaL)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntEi(-I*(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaL)*t)
                    -(I*pade_zeta(l)/betaL)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntEi(I*(cx_double(cutoff,0.0) - I*pade_zeta(l)/betaL)*t))

                   -(cx_double(1.0,0.0)/(conj(EffEvalL(k)) - I*pade_zeta(l)/betaL))
                   *(conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t)
                    -conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t)
                    +(I*pade_zeta(l)/betaL)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntEi(-I*(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaL)*t)
                    -(I*pade_zeta(l)/betaL)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntEi(I*(cx_double(cutoff,0.0) - I*pade_zeta(l)/betaL)*t))

                    )

                  +(pade_eta(l)/betaR)*GammaRightEff_basis(j,k)
                  *(

                    (cx_double(1.0,0.0)/(EffEvalL(j) + I*pade_zeta(l)/betaR))
                   *(EffEvalL(j)*ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
                    -EffEvalL(j)*ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
                    -EffEvalL(j)*2.0*math::pi()*I
                    +(I*pade_zeta(l)/betaR)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntE(I*(cx_double(cutoff,0.0)-I*pade_zeta(l)/betaR)*t)
                    -(I*pade_zeta(l)/betaR)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntE(-I*(cx_double(cutoff,0.0)+I*pade_zeta(l)/betaR)*t))

                   -(cx_double(1.0,0.0)/(conj(EffEvalL(k)) + I*pade_zeta(l)/betaR))
                   *(conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t)
                    -conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t)
                    +(I*pade_zeta(l)/betaR)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntE(I*(cx_double(cutoff,0.0)-I*pade_zeta(l)/betaR)*t)
                    -(I*pade_zeta(l)/betaR)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntE(-I*(cx_double(cutoff,0.0)+I*pade_zeta(l)/betaR)*t))

                   +(cx_double(1.0,0.0)/(EffEvalL(j) - I*pade_zeta(l)/betaR))
                   *(EffEvalL(j)*ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
                    -EffEvalL(j)*ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
                    -EffEvalL(j)*2.0*math::pi()*I
                    +(I*pade_zeta(l)/betaR)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntEi(-I*(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaR)*t)
                    -(I*pade_zeta(l)/betaR)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntEi(I*(cx_double(cutoff,0.0) - I*pade_zeta(l)/betaR)*t))

                   -(cx_double(1.0,0.0)/(conj(EffEvalL(k)) - I*pade_zeta(l)/betaR))
                   *(conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t)
                    -conj(EffEvalL(k))*exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t)
                    +(I*pade_zeta(l)/betaR)*exp(-I*(cx_double(cutoff,0.0)+EffEvalL(j))*t)*ExpExpIntEi(-I*(cx_double(cutoff,0.0) + I*pade_zeta(l)/betaR)*t)
                    -(I*pade_zeta(l)/betaR)*exp(I*(cx_double(cutoff,0.0)-EffEvalL(j))*t)*ExpExpIntEi(I*(cx_double(cutoff,0.0) - I*pade_zeta(l)/betaR)*t))

                    );
    }

    return (cx_double(1.0,0.0)/(2.0*math::pi()*(EffEvalL(j) - conj(EffEvalL(k)))))
          *( (cx_double(1.0,0.0)/betaL)*GammaLeftEff_basis(j,k)
            *(ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
             -ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
             -2.0*math::pi()*I
             -exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t)
             +exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t)
             )

            -0.5*GammaLeftEff_basis(j,k)
            *(EffEvalL(j)*ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
             -EffEvalL(j)*ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
             -EffEvalL(j)*2.0*math::pi()*I
             -exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*conj(EffEvalL(k))*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t) 
             +exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*conj(EffEvalL(k))*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t))

            +(cx_double(1.0,0.0)/betaR)*GammaRightEff_basis(j,k)
            *(ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
             -ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
             -2.0*math::pi()*I
             -exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t)
             +exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t)
             )

            -0.5*GammaRightEff_basis(j,k)
            *(EffEvalL(j)*ExpIntEi(I*(cx_double(cutoff,0.0) - EffEvalL(j))*t)
             -EffEvalL(j)*ExpIntEi(-I*(cx_double(cutoff,0.0) + EffEvalL(j))*t)
             -EffEvalL(j)*2.0*math::pi()*I
             -exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*conj(EffEvalL(k))*ExpIntEi(I*(cx_double(cutoff,0.0) - conj(EffEvalL(k)))*t) 
             +exp(-I*(EffEvalL(j)-conj(EffEvalL(k)))*t)*conj(EffEvalL(k))*ExpIntEi(-I*(cx_double(cutoff,0.0) + conj(EffEvalL(k)))*t))

            +tempsum

            );
}
