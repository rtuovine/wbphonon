#include "Main.hpp"
void TDCalculation(int Nc, int Nt, int subs, int s, int terminal, double dt, double range, double cutoff, 
                   double betaL, double betaR, double betaC, double klambda, double klambdaC, double acc, 
                   cx_vec EffEvalL, cx_mat EffEvecL, cx_vec EffEvalR, cx_mat EffEvecR, 
                   cx_mat Omega0Tilde, cx_mat OmegaTilde, cx_mat OmegaTildeTest, 
                   cx_mat LambdaLeft, cx_mat LambdaRight, cx_mat GammaLeft, cx_mat GammaRight, 
                   cx_mat alpha, cx_mat OmegaEff, cx_mat OmegaEffDagger, cx_mat &D_lss_ss, 
                   cx_mat &D_lss_ss2, cx_cube &D_lss, cx_mat &jQLeft, cx_mat &jQRight, 
                   cx_vec &jQLeft_ss, cx_vec &jQRight_ss, cx_vec &jQLeft_ss2, cx_vec &jQRight_ss2, 
                   cx_vec &EQ, cx_vec &dEQ, cx_vec Mass, cx_mat &LocalT, cx_mat &dLocalT,
                   int sol, int padeorder, vec pade_eta, vec pade_zeta,
                   cx_mat &Eloc, cx_mat &dEloc)
{
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int elems = Nt/size;
    cx_double I(0.0,1.0);

    // GSL-functions, workspaces, variables, etc...
    gsl_function F1re, F2re, F3re, F1im, F2im, F3im;
    gsl_integration_workspace *ws1re, *ws2re, *ws3re, *ws1im, *ws2im, *ws3im;
    double result1re, result2re, result3re, error1re, error2re, error3re, 
           result1im, result2im, result3im, error1im, error2im, error3im;

    const double epsabs = acc;          // Integration convergence (absolute)
    const double epsrel = acc;          // Integration convergence (relative)
    const size_t wslimit = subs;        // Worspace allocation
    const double lo = -range*cutoff;    // Lower limit for integration (should, in principle, be -infty)
    const double up = range*cutoff;     // Upper limit for integration (should, in principle, be +infty)
    const int key = 1;                  // Integration rule for gaq \in {1, ... , 6}

    // Singular points for GSL integration
    size_t npts = 2*Nc+2;
    double pts[npts];
    if(s == 1)
    {
        // Sort eigenvalues (real parts) into ascending order
        double temparray[2*Nc];
        for(int j=0; j<2*Nc; j++) temparray[j] = real(EffEvalR(j));
        double a;
        for (int i=0; i<2*Nc; ++i)
        {
            for (int j=i+1; j<2*Nc; ++j)
            {
                if (temparray[i] > temparray[j])
                {
                    a = temparray[i];
                    temparray[i] = temparray[j];
                    temparray[j] = a;
                }
            }
        }
        // Write the endpoints of the integration and singular points into pts
        for(int j=0; j<2*Nc+2; j++)
        {
            if(j==0) pts[j] = lo;
            else if(j<2*Nc+1) pts[j] = temparray[j-1];
            else pts[j] = up;
        }
    }

    cx_mat GammaLeftEff(2*Nc,2*Nc), GammaRightEff(2*Nc,2*Nc);
    cx_cube tevol(2*Nc,2*Nc,Nt/size), init(2*Nc,2*Nc,Nt/size);
    GammaLeftEff.zeros();
    GammaRightEff.zeros();
    tevol.zeros();
    init.zeros();

    GammaLeftEff = inv(alpha + 0.5*I*(GammaLeft + GammaRight))
                  *GammaLeft*trans(inv(alpha + 0.5*I*(GammaLeft + GammaRight)));
    GammaRightEff = inv(alpha + 0.5*I*(GammaLeft + GammaRight))
                   *GammaRight*trans(inv(alpha + 0.5*I*(GammaLeft + GammaRight)));

    cx_cube temp_cube(2*Nc,2*Nc,Nt/size);
    temp_cube.zeros();
    cx_double jj;
    for(int i=0; i<elems; i++)
    {
        int j = elems*rank + i;
        jj = cx_double(static_cast<double>(j),0.0);
        for(int k=0; k<2*Nc; k++)
        {
            temp_cube(k,k,i) = exp(-I*EffEvalR(k)*jj*dt);
        }
    }
    for(int i=0; i<elems; i++)
    {
        tevol.slice(i) = EffEvecR*diagmat(temp_cube.slice(i))*inv(EffEvecR);
    }

    for(int i=0; i<elems; i++)
    {
//        init.slice(i) = -I*tevol.slice(i)*alpha*Bose_mat(betaC,OmegaTildeTest*alpha)
//                        *trans(tevol.slice(i));
        init.slice(i) = -I*tevol.slice(i)*alpha*Bose_mat(betaC,OmegaTilde*alpha)
                        *trans(tevol.slice(i));
    }

    cx_mat res1(2*Nc,2*Nc), res2(2*Nc,2*Nc), res3(2*Nc,2*Nc), tevolj(2*Nc,2*Nc);
    tevolj.zeros();

// Spectral functions
    if(rank == 0)
    {
        cout << endl; cout << "Spectral functions..." << endl;
        cout << endl;
        double dw = min(abs(imag(EffEvalR)))/4.0;
        int Nw = ceil((up-lo)/dw);
        if(Nw > 10000) {Nw = 9999; dw = (up-lo)/Nw;}
        double ww = lo;
        cx_mat DR(2*Nc,2*Nc), DA(2*Nc,2*Nc), DRexact(2*Nc,2*Nc), DAexact(2*Nc,2*Nc), BLeft(2*Nc,2*Nc), BLeftexact(2*Nc,2*Nc), BRight(2*Nc,2*Nc), BRightexact(2*Nc,2*Nc);;
        cx_mat LambdaLeft_tmp(2*Nc,2*Nc), LambdaRight_tmp(2*Nc,2*Nc), GammaLeft_tmp(2*Nc,2*Nc), GammaRight_tmp(2*Nc,2*Nc);
        cx_mat temp(2*Nc,2*Nc);
        LambdaLeft_tmp.zeros();
        LambdaRight_tmp.zeros();
        GammaLeft_tmp.zeros();
        GammaRight_tmp.zeros();
        ofstream dretre("output/Dret_WBA_Re.out", ios::out);
        ofstream dretexre("output/Dret_exact_Re.out", ios::out);
        ofstream BLeft_WBAre("output/BLeft_WBA_Re.out", ios::out);
        ofstream BLeft_exactre("output/BLeft_exact_Re.out", ios::out);
        ofstream BRight_WBAre("output/BRight_WBA_Re.out", ios::out);
        ofstream BRight_exactre("output/BRight_exact_Re.out", ios::out);
        ofstream dretim("output/Dret_WBA_Im.out", ios::out);
        ofstream dretexim("output/Dret_exact_Im.out", ios::out);
        ofstream BLeft_WBAim("output/BLeft_WBA_Im.out", ios::out);
        ofstream BLeft_exactim("output/BLeft_exact_Im.out", ios::out);
        ofstream BRight_WBAim("output/BRight_WBA_Im.out", ios::out);
        ofstream BRight_exactim("output/BRight_exact_Im.out", ios::out);
        for(unsigned int i=0; i<Nw; i++)
        {
            if(terminal == 0)
            {
                DR = inv(ww*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*ww*(GammaLeft + GammaRight))*alpha;
                //DR = inv(ww*eye(2*Nc,2*Nc) - OmegaEff)*inv(alpha + 0.5*I*(GammaLeft + GammaRight));
                DA = alpha*inv(ww*eye(2*Nc,2*Nc) - OmegaTilde*alpha - (LambdaLeft + LambdaRight)*alpha - 0.5*I*ww*(GammaLeft + GammaRight)*alpha);
                //DA = inv(alpha - 0.5*I*(GammaLeft + GammaRight))*inv(ww*eye(2*Nc,2*Nc) - OmegaEffDagger);
                //BLeft = DR*ww*GammaLeft*trans(DR);
                BLeft = DR*ww*GammaLeft*DA;
                //BRight = DR*ww*GammaRight*trans(DR);
                BRight = DR*ww*GammaRight*DA;
                LambdaLeft_tmp(0,0) = -((klambdaC*klambdaC)/(2.0*klambda*klambda))*(2.0*klambda - ww*ww);
                LambdaRight_tmp(Nc-1,Nc-1) = -((klambdaC*klambdaC)/(2.0*klambda*klambda))*(2.0*klambda - ww*ww);
                GammaLeft_tmp(0,0) = ((klambdaC*klambdaC)/(klambda*klambda))*sqrt(abs(ww*ww - 4.0*klambda));
                GammaRight_tmp(Nc-1,Nc-1) = ((klambdaC*klambdaC)/(klambda*klambda))*sqrt(abs(ww*ww - 4.0*klambda));
                DRexact = inv(ww*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft_tmp + LambdaRight_tmp) + 0.5*I*alpha*ww*(GammaLeft_tmp + GammaRight_tmp))*alpha;
                DAexact = alpha*inv(ww*eye(2*Nc,2*Nc) - OmegaTilde*alpha - (LambdaLeft_tmp + LambdaRight_tmp)*alpha - 0.5*I*ww*(GammaLeft_tmp + GammaRight_tmp)*alpha);
                //BLeftexact = DRexact*ww*GammaLeft_tmp*trans(DRexact);
                BLeftexact = DRexact*ww*GammaLeft_tmp*DAexact;
                //BRightexact = DRexact*ww*GammaRight_tmp*trans(DRexact);
                BRightexact = DRexact*ww*GammaRight_tmp*DAexact;
            }
            else if(terminal == 1)
            {
                DR = inv(ww*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft + LambdaRight) + 0.5*I*alpha*ww*(GammaLeft + GammaRight))*alpha;
                //DR = inv(ww*eye(2*Nc,2*Nc) - OmegaEff)*inv(alpha + 0.5*I*(GammaLeft + GammaRight));
                DA = alpha*inv(ww*eye(2*Nc,2*Nc) - OmegaTilde*alpha - (LambdaLeft + LambdaRight)*alpha - 0.5*I*ww*(GammaLeft + GammaRight)*alpha);
                //DA = inv(alpha - 0.5*I*(GammaLeft + GammaRight))*inv(ww*eye(2*Nc,2*Nc) - OmegaEffDagger);
                //BLeft = DR*ww*GammaLeft*trans(DR);
                BLeft = DR*ww*GammaLeft*DA;
                //BRight = DR*ww*GammaRight*trans(DR);
                BRight = DR*ww*GammaRight*DA;
                LambdaLeft_tmp(0,0) = -(klambda - 0.5*ww*ww);
                LambdaRight_tmp(Nc-1,Nc-1) = -(klambda - 0.5*ww*ww);
                GammaLeft_tmp(0,0) = sqrt(abs(ww*ww - 4.0*klambda));
                GammaRight_tmp(Nc-1,Nc-1) = sqrt(abs(ww*ww - 4.0*klambda));
                DRexact = inv(ww*eye(2*Nc,2*Nc) - alpha*OmegaTilde - alpha*(LambdaLeft_tmp + LambdaRight_tmp) + 0.5*I*alpha*ww*(GammaLeft_tmp + GammaRight_tmp))*alpha;
                DAexact = alpha*inv(ww*eye(2*Nc,2*Nc) - OmegaTilde*alpha - (LambdaLeft_tmp + LambdaRight_tmp)*alpha - 0.5*I*ww*(GammaLeft_tmp + GammaRight_tmp)*alpha);
                //BLeftexact = DRexact*ww*GammaLeft_tmp*trans(DRexact);
                BLeftexact = DRexact*ww*GammaLeft_tmp*DAexact;
                //BRightexact = DRexact*ww*GammaRight_tmp*trans(DRexact);
                BRightexact = DRexact*ww*GammaRight_tmp*DAexact;
            }
            else {cout << "Erroneous terminal parameter in TDCalculation.cpp, exiting ..." << endl; exit(1); }

            dretre << setprecision(5) << scientific << ww << " ";
            dretexre << setprecision(5) << scientific << ww << " ";
            BLeft_WBAre << setprecision(5) << scientific << ww << " ";
            BLeft_exactre << setprecision(5) << scientific << ww << " ";
            BRight_WBAre << setprecision(5) << scientific << ww << " ";
            BRight_exactre << setprecision(5) << scientific << ww << " ";
            dretim << setprecision(5) << scientific << ww << " ";
            dretexim << setprecision(5) << scientific << ww << " ";
            BLeft_WBAim << setprecision(5) << scientific << ww << " ";
            BLeft_exactim << setprecision(5) << scientific << ww << " ";
            BRight_WBAim << setprecision(5) << scientific << ww << " ";
            BRight_exactim << setprecision(5) << scientific << ww << " ";
            for(unsigned int j=0; j<2*Nc; j++)
            {
                for(unsigned int k=0; k<2*Nc; k++)
                {
                    dretre << real(DR(j,k)) << " ";
                    dretexre << real(DRexact(j,k)) << " ";
                    BLeft_WBAre << real(BLeft(j,k)) << " ";
                    BLeft_exactre << real(BLeftexact(j,k)) << " ";
                    BRight_WBAre << real(BRight(j,k)) << " ";
                    BRight_exactre << real(BRightexact(j,k)) << " ";
                    dretim << imag(DR(j,k)) << " ";
                    dretexim << imag(DRexact(j,k)) << " ";
                    BLeft_WBAim << imag(BLeft(j,k)) << " ";
                    BLeft_exactim << imag(BLeftexact(j,k)) << " ";
                    BRight_WBAim << imag(BRight(j,k)) << " ";
                    BRight_exactim << imag(BRightexact(j,k)) << " ";
                }
            }
            dretre << endl;
            dretexre << endl;
            BLeft_WBAre << endl;
            BLeft_exactre << endl;
            BRight_WBAre << endl;
            BRight_exactre << endl;
            dretim << endl;
            dretexim << endl;
            BLeft_WBAim << endl;
            BLeft_exactim << endl;
            BRight_WBAim << endl;
            BRight_exactim << endl;
            ww += dw;
        }
        dretre.close();
        dretexre.close();
        BLeft_WBAre.close();
        BLeft_exactre.close();
        BRight_WBAre.close();
        BRight_exactre.close();
        dretim.close();
        dretexim.close();
        BLeft_WBAim.close();
        BLeft_exactim.close();
        BRight_WBAim.close();
        BRight_exactim.close();
    }

// Time-dependent calculation
    if(rank == 0)
    {
        cout << endl; cout << "Time-dependent calculation..." << endl;
        cout << endl;
    }

    if(sol==1) // Numerical integration
    {
        int status;
        double epsabsvar, epsrelvar;

        double lovar, upvar;

        for(int i=0; i<elems; i++)     // Time loop
        {
            int j = elems*rank + i;    // The for-loop (i) runs up to divided steps, 
                                       // index j accounts for individual steps

            double jj(static_cast<double>(j));
            lovar = -range*cutoff*(1.0 + sqrt(abs(Nt*dt*0.5 - jj*dt))*Theta(Nt*dt*0.5 - jj*dt));
            upvar = range*cutoff*(1.0 + sqrt(abs(Nt*dt*0.5 - jj*dt))*Theta(Nt*dt*0.5 - jj*dt));

            // For each time step, calculate different integral-sum
            res1.zeros();
            res2.zeros();
            res3.zeros();
            tevolj = tevol.slice(i);

            // Integrations are done for each matrix element 
            for(int p=0; p<2*Nc; p++)
            {
                for(int q=0; q<=p; q++)
                {
                    // The parameters for the integrations are stored in a structure 'Params'
                    Params para;
                    para.j = j;
                    para.p = p;
                    para.q = q;
                    para.betaL = betaL;
                    para.betaR = betaR;
                    para.OmegaEff = OmegaEff;
                    para.OmegaEffDagger = OmegaEffDagger;
                    para.tevolj = tevolj;
                    para.GammaLeftEff = GammaLeftEff;
                    para.GammaRightEff = GammaRightEff;
                    para.Nc = Nc;
                    para.dt = dt;

                    Params2 para2;
                    para2.p = p;
                    para2.q = q;
                    para2.betaL = betaL;
                    para2.betaR = betaR;
                    para2.klambda = klambda;
                    para2.alpha = alpha;
                    para2.OmegaTilde = OmegaTilde;
                    para2.Nc = Nc;
                    para2.terminal = terminal;
                    para2.klambdaC = klambdaC;

                    // Integrand is defined in a separate function
                    F1re.function = &Integrand1re;
                    // Parameters are sent in as one 'bunch'
                    F1re.params = reinterpret_cast<void *>(&para);
                    // Workspace allocation
                    ws1re = gsl_integration_workspace_alloc(wslimit);
                    // Tune GSL's error handler to take different tolerances into account
                    gsl_error_handler_t *old_handler1 = gsl_set_error_handler_off();
                    status = 1;
                    epsabsvar = epsabs;
                    epsrelvar = epsrel;
                    while(status)   // Endless loop unless status=0; GSL returns 0 when successful
                    {
                        if(s == 1)
                        {
                            status = gsl_integration_qagp(&F1re, pts, npts, epsabsvar, epsrelvar, 
                                                          wslimit, ws1re, &result1re, &error1re);
                        }
                        else
                        {
    //                        status = gsl_integration_qag(&F1re, lovar, upvar, epsabsvar, epsrelvar, wslimit, 
    //                                                     key, ws1re, &result1re, &error1re);
                            status = gsl_integration_qag(&F1re, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                         key, ws1re, &result1re, &error1re);
                        }
                        epsabsvar *= 1.1;
                        epsrelvar *= 1.1;

                        if(status) {cout << setprecision(3) << scientific
                                         << "\tProblems in integration; increasing tolerance to "
                                         << epsabsvar << endl;}
                    }
                    gsl_set_error_handler(old_handler1);
                    gsl_integration_workspace_free(ws1re);

                    // Same as above for the imaginary part
                    F1im.function = &Integrand1im;
                    F1im.params = reinterpret_cast<void *>(&para);
                    ws1im = gsl_integration_workspace_alloc(wslimit);
                    gsl_error_handler_t *old_handler2 = gsl_set_error_handler_off();
                    status = 1;
                    epsabsvar = epsabs;
                    epsrelvar = epsrel;
                    while(status)
                    {
                        if(s == 1)
                        {
                            status = gsl_integration_qagp(&F1im, pts, npts, epsabsvar, epsrelvar, 
                                                          wslimit, ws1im, &result1im, &error1im);
                        }
                        else
                        {
    //                        status = gsl_integration_qag(&F1im, lovar, upvar, epsabsvar, epsrelvar, wslimit, 
    //                                                     key, ws1im, &result1im, &error1im);
                            status = gsl_integration_qag(&F1im, lo, up, epsabsvar, epsrelvar, wslimit, 
                                                         key, ws1im, &result1im, &error1im);
                        }
                        epsabsvar *= 1.1;
                        epsrelvar *= 1.1;

                        if(status) {cout << setprecision(3) << scientific
                                         << "\tProblems in integration; increasing tolerance to "
                                         << epsabsvar << endl;}
                    }
                    gsl_set_error_handler(old_handler2);
                    gsl_integration_workspace_free(ws1im);

                    // Same as above for the steady-state results
                    if(j==0)
                    {
                        // Real part
                        F2re.function = &Integrand2re;
                        F2re.params = reinterpret_cast<void *>(&para);
                        ws2re = gsl_integration_workspace_alloc(wslimit);
                        gsl_error_handler_t *old_handler3 = gsl_set_error_handler_off();
                        status = 1;
                        epsabsvar = epsabs;
                        epsrelvar = epsrel;
                        while(status)
                        {
                            if(s == 1)
                            {
                                status = gsl_integration_qagp(&F2re, pts, npts, epsabsvar, epsrelvar, 
                                                              wslimit, ws2re, &result2re, &error2re);
                            }
                            else
                            {
    //                            status = gsl_integration_qag(&F2re, lovar, upvar, epsabsvar, epsrelvar, 
    //                                                         wslimit, key, ws2re, &result2re, &error2re);
                                status = gsl_integration_qag(&F2re, lo, up, epsabsvar, epsrelvar, 
                                                             wslimit, key, ws2re, &result2re, &error2re);
                            }
                            epsabsvar *= 1.1;
                            epsrelvar *= 1.1;

                            if(status) {cout << setprecision(3) << scientific
                                             << "\tProblems in integration; increasing tolerance to "
                                             << epsabsvar << endl;}
                        }
                        gsl_set_error_handler(old_handler3);
                        gsl_integration_workspace_free(ws2re);

                        // Imaginary part
                        F2im.function = &Integrand2im;
                        F2im.params = reinterpret_cast<void *>(&para);
                        ws2im = gsl_integration_workspace_alloc(wslimit);
                        gsl_error_handler_t *old_handler4 = gsl_set_error_handler_off();
                        status = 1;
                        epsabsvar = epsabs;
                        epsrelvar = epsrel;
                        while(status)
                        {
                            if(s == 1)
                            {
                                status = gsl_integration_qagp(&F2im, pts, npts, epsabsvar, epsrelvar, 
                                                              wslimit, ws2im, &result2im, &error2im);
                            }
                            else
                            {
    //                            status = gsl_integration_qag(&F2im, lovar, upvar, epsabsvar, epsrelvar, 
    //                                                         wslimit, key, ws2im, &result2im, &error2im);
                                status = gsl_integration_qag(&F2im, lo, up, epsabsvar, epsrelvar, 
                                                             wslimit, key, ws2im, &result2im, &error2im);
                            }
                            epsabsvar *= 1.1;
                            epsrelvar *= 1.1;

                            if(status) {cout << setprecision(3) << scientific
                                             << "\tProblems in integration; increasing tolerance to "
                                             << epsabsvar << endl;}
                        }
                        gsl_set_error_handler(old_handler4);
                        gsl_integration_workspace_free(ws2im);

                        //  Beyond WBA
                        // Real part
                        F3re.function = &Integrand3re;
                        F3re.params = reinterpret_cast<void *>(&para2);
                        ws3re = gsl_integration_workspace_alloc(wslimit);
                        gsl_error_handler_t *old_handler5 = gsl_set_error_handler_off();
                        status = 1;
                        epsabsvar = epsabs;
                        epsrelvar = epsrel;
                        while(status)
                        {
                            status = gsl_integration_qag(&F3re, lo, up, epsabsvar, epsrelvar, 
                                                         wslimit, key, ws3re, &result3re, &error3re);

                            epsabsvar *= 1.1;
                            epsrelvar *= 1.1;

                            if(status) {cout << setprecision(3) << scientific
                                             << "\tProblems in integration; increasing tolerance to "
                                             << epsabsvar << endl;}
                        }
                        gsl_set_error_handler(old_handler5);
                        gsl_integration_workspace_free(ws3re);

                        // Imaginary part
                        F3im.function = &Integrand3im;
                        F3im.params = reinterpret_cast<void *>(&para2);
                        ws3im = gsl_integration_workspace_alloc(wslimit);
                        gsl_error_handler_t *old_handler6 = gsl_set_error_handler_off();
                        status = 1;
                        epsabsvar = epsabs;
                        epsrelvar = epsrel;
                        while(status)
                        {
                            status = gsl_integration_qag(&F3im, lo, up, epsabsvar, epsrelvar, 
                                                         wslimit, key, ws3im, &result3im, &error3im);
              
                            epsabsvar *= 1.1;
                            epsrelvar *= 1.1;

                            if(status) {cout << setprecision(3) << scientific
                                             << "\tProblems in integration; increasing tolerance to "
                                             << epsabsvar << endl;}
                        }
                        gsl_set_error_handler(old_handler6);
                        gsl_integration_workspace_free(ws3im);
                    }

                    // Write integration results into temp matrices
                    res1(p,q) = cx_double(result1re,result1im);
                    res1(q,p) = conj(cx_double(result1re,result1im));

                    if(j==0)
                    {
                        res2(p,q) = cx_double(result2re,result2im);
                        res2(q,p) = conj(cx_double(result2re,result2im));
                        res3(p,q) = cx_double(result3re,result3im);
                        res3(q,p) = conj(cx_double(result3re,result3im));
                    }
                } // for q
            } // for p

            // Both time-dependent and steady-state (full) density matrices are then calculated from res1 and res2
            D_lss.slice(i) = res1/(2.0*math::pi()) + I*init.slice(i);
            //D_lss.slice(i) = I*init.slice(i);
            if(j==0)
            {
                D_lss_ss = res2/(2.0*math::pi());
                D_lss_ss2 = res3/(2.0*math::pi());
            }
        } // for i
    } // endif(sol==1)

    else if(sol==2) // Analytic formula in terms of the Padé parameters (TO BE TESTED)
    {
        cx_mat GammaLeftEff_basis(2*Nc,2*Nc);
        cx_mat GammaRightEff_basis(2*Nc,2*Nc);
        cx_mat Init_basis(2*Nc,2*Nc);
        GammaLeftEff_basis.zeros();
        GammaRightEff_basis.zeros();
        Init_basis.zeros();
        PadeExpansion(padeorder, pade_eta, pade_zeta);

        cx_mat one(2*Nc,2*Nc);
        one.zeros();

        cx_double tmpcdble(0.0,0.0);
        cx_double tmpcdble2(0.0,0.0);
        cx_double temp1, temp2;

        GammaLeftEff_basis = trans(EffEvecL)*GammaLeftEff*EffEvecL;
        GammaRightEff_basis = trans(EffEvecL)*GammaRightEff*EffEvecL;
        Init_basis = trans(EffEvecL)*alpha*Bose_mat(betaC,OmegaTilde*alpha)*EffEvecL;

        for(unsigned int j=0; j<2*Nc; j++)
        {
            for(unsigned int k=0; k<=j; k++)
            {
                Params3 para3;
                para3.j = j;
                para3.k = k;
                para3.padeorder = padeorder;
                para3.betaL = betaL;
                para3.betaR = betaR;
                para3.cutoff = cutoff;
                para3.pade_eta = pade_eta;
                para3.pade_zeta = pade_zeta;
                para3.EffEvalL = EffEvalL;
                para3.GammaLeftEff_basis = GammaLeftEff_basis;
                para3.GammaRightEff_basis = GammaRightEff_basis;

                tmpcdble = OneEvaluate(reinterpret_cast<void *>(&para3));

                if(isnan(tmpcdble) == true || isinf(tmpcdble) == true)  // Check divergences
                {
                    if(rank == 0)
                    {
                        cout << "Error in One!" << endl;
                    }
                }

                one(j,k) = tmpcdble;
                one(k,j) = conj(tmpcdble);
                tmpcdble = cx_double(0.0,0.0);

            }   // k<=j
        }   // j<2*Nc

        for(unsigned int i=0; i<elems; i++)                 // Time loop
        {
            for(unsigned int j=0; j<2*Nc; j++)
            {
                for(unsigned int k=0; k<=j; k++)    // Symmetry
                {
                    Params3 para4;
                    para4.j = j;
                    para4.k = k;
                    para4.padeorder = padeorder;
                    para4.betaL = betaL;
                    para4.betaR = betaR;
                    para4.cutoff = cutoff;
                    para4.pade_eta = pade_eta;
                    para4.pade_zeta = pade_zeta;
                    para4.EffEvalL = EffEvalL;
                    para4.GammaLeftEff_basis = GammaLeftEff_basis;
                    para4.GammaRightEff_basis = GammaRightEff_basis;

                    tmpcdble = TwoEvaluate((elems*rank + i)*dt+1.0e-15,reinterpret_cast<void *>(&para4));

                    if(isnan(tmpcdble) == true || isinf(tmpcdble) == true)  // Check divergences
                    {
                        if(rank == 0)
                        {
                            cout << "Error in Two!" << endl;
                        }
                    }

                    Params3 para5;
                    para5.j = k;
                    para5.k = j;
                    para5.padeorder = padeorder;
                    para5.betaL = betaL;
                    para5.betaR = betaR;
                    para5.cutoff = cutoff;
                    para5.pade_eta = pade_eta;
                    para5.pade_zeta = pade_zeta;
                    para5.EffEvalL = EffEvalL;
                    para5.GammaLeftEff_basis = GammaLeftEff_basis;
                    para5.GammaRightEff_basis = GammaRightEff_basis;

                    tmpcdble2 = TwoEvaluate((elems*rank + i)*dt+1.0e-15,reinterpret_cast<void *>(&para5));

                    if(isnan(tmpcdble2) == true || isinf(tmpcdble2) == true)  // Check divergences
                    {
                        if(rank == 0)
                        {
                            cout << "Error in Two!" << endl;
                        }
                    }

                    temp1 = exp(-I*(EffEvalL(j) - conj(EffEvalL(k)))*static_cast<double>((elems*rank + i)*dt+1.0e-15))*Init_basis(j,k)
                           + one(j,k)
                           - tmpcdble
                           - conj(tmpcdble2)
                           + exp(-I*(EffEvalL(j) - conj(EffEvalL(k)))*static_cast<double>((elems*rank + i)*dt+1.0e-15))*one(j,k);

                    if(elems*rank + i == 0)
                    {
                        temp2 = one(j,k);
                    }

                    tmpcdble = cx_double(0.0,0.0);
                    tmpcdble2 = cx_double(0.0,0.0);

                    if(isnan(temp1) == true || isinf(temp1) == true)    // Check divergences
                    {
                        if(rank == 0)
                        {
                            cout << "Error in rho!" << endl;
                        }
                    }

                    if(isnan(temp2) == true || isinf(temp2) == true)    // Check divergences
                    {
                        if(rank == 0)
                        {
                            cout << "Error in rho_ss!" << endl;
                        }
                    }

                    D_lss(j,k,i) = temp1;
                    D_lss(k,j,i) = conj(temp1);
                            
                    if(elems*rank + i == 0)
                    {
                        D_lss_ss(j,k) = temp2;
                        D_lss_ss(k,j) = conj(temp2);
                    }
                }   // k<=j
            }   // j<2*Nc
        } // time loop

// Basis transformation

        cx_mat D_lss_ss_basis(2*Nc,2*Nc);
        cx_cube D_lss_basis(2*Nc,2*Nc,Nt/size);

        D_lss_basis = D_lss;
        D_lss_ss_basis = D_lss_ss;

        cx_mat overlap_lr(2*Nc,2*Nc), invover_lr(2*Nc,2*Nc), 
               overlap_rl(2*Nc,2*Nc), invover_rl(2*Nc,2*Nc);

        overlap_lr = trans(EffEvecL)*EffEvecR;
        overlap_rl = trans(EffEvecR)*EffEvecL;
        invover_lr = inv(overlap_lr);
        invover_rl = inv(overlap_rl);

        for(unsigned int i=0; i<elems; i++)
        {
            for(unsigned int m=0; m<2*Nc; m++)
            {
                for(unsigned int n=0; n<=m; n++)    // Symmetry
                {
                    temp1 = cx_double(0.0,0.0);
                    temp2 = cx_double(0.0,0.0);

                    //if(m == n || real(OmegaEff(m,n)) != 0.0 || imag(OmegaEff(m,n)) != 0.0) // Optimization! Should speed things up but needs testing...
                    {
                        for(unsigned int j=0; j<2*Nc; j++)
                        {
                            for(unsigned int k=0; k<2*Nc; k++)
                            {
                                temp1 += EffEvecR(m,j)*invover_lr(j,j)
                                        *conj(EffEvecR(n,k))*invover_rl(k,k)
                                        *D_lss_basis.slice(i)(j,k);

                                if(elems*rank + i == 0)
                                {
                                    temp2 += EffEvecR(m,j)*invover_lr(j,j)
                                            *conj(EffEvecR(n,k))*invover_rl(k,k)
                                            *D_lss_ss_basis(j,k);
                                }

                            }   // k<nsite
                        }   // j<nsite

                        D_lss.slice(i)(m,n) = temp1;
                        D_lss.slice(i)(n,m) = conj(temp1);

                        if(elems*rank + i == 0)
                        {
                            D_lss_ss(m,n) = temp2;
                            D_lss_ss(n,m) = conj(temp2);
                        }
                    }   // if m==n etc
                }   // n<=m
            }   // m<nsite
        }   // i<elems

    } // if(sol==2)
    else{cout << "Erroneous solution parameter in TDCalculation.cpp, exiting ..." << endl; exit(1);}

// UP transformation (this is not needed if the mass matrix is a unit matrix)
	for(int i=0; i<elems; i++)
	{
        int j = elems*rank + i;
        for(int p=0; p<2*Nc; p++)
        {
            for(int q=0; q<2*Nc; q++)
            {
                if(j == 0)
                {
                    if(p < Nc && q < Nc)
                    {
                        D_lss_ss(p,q) = D_lss_ss(p,q)/(sqrt(Mass(p))*sqrt(Mass(q)));
                        D_lss_ss2(p,q) = D_lss_ss2(p,q)/(sqrt(Mass(p))*sqrt(Mass(q)));
                    }
                    else if(p < Nc && q >= Nc)
                    {
                        D_lss_ss(p,q) = (D_lss_ss(p,q)/(sqrt(Mass(p))))*sqrt(Mass(q-Nc));
                        D_lss_ss2(p,q) = (D_lss_ss2(p,q)/(sqrt(Mass(p))))*sqrt(Mass(q-Nc));
                    }
                    else if(p >= Nc && q < Nc)
                    {
                        D_lss_ss(p,q) = (D_lss_ss(p,q)/(sqrt(Mass(q))))*sqrt(Mass(p-Nc));
                        D_lss_ss2(p,q) = (D_lss_ss2(p,q)/(sqrt(Mass(q))))*sqrt(Mass(p-Nc));
                    }
                    else
                    {
                        D_lss_ss(p,q) = D_lss_ss(p,q)*(sqrt(Mass(p-Nc))*sqrt(Mass(q-Nc)));
                        D_lss_ss2(p,q) = D_lss_ss2(p,q)*(sqrt(Mass(p-Nc))*sqrt(Mass(q-Nc)));
                    }
                }

                if(p < Nc && q < Nc)
                {
                    D_lss.slice(i)(p,q) = D_lss.slice(i)(p,q)/(sqrt(Mass(p))*sqrt(Mass(q)));
                }
                else if(p < Nc && q >= Nc)
                {
                    D_lss.slice(i)(p,q) = (D_lss.slice(i)(p,q)/(sqrt(Mass(p))))*sqrt(Mass(q-Nc));
                }
                else if(p >= Nc && q < Nc)
                {
                    D_lss.slice(i)(p,q) = (D_lss.slice(i)(p,q)/(sqrt(Mass(q))))*sqrt(Mass(p-Nc));
                }
                else
                {
                    D_lss.slice(i)(p,q) = D_lss.slice(i)(p,q)*(sqrt(Mass(p-Nc))*sqrt(Mass(q-Nc)));
                }
            }
        }
    }

// Heat currents, energies and temperatures
    cx_double tmpcdble(0.0,0.0);
    if(Nc > 1)
    {
        for(int j=0; j<Nc-1; j++)
        {
            jQLeft_ss(j) = (Omega0Tilde(j,j+1)/Mass(j+1))*D_lss_ss(j,Nc+j+1);
            jQRight_ss(j) = -(Omega0Tilde(j+1,j)/Mass(j+1))*D_lss_ss(Nc+j+1,j);
            jQLeft_ss2(j) = (Omega0Tilde(j,j+1)/Mass(j+1))*D_lss_ss2(j,Nc+j+1);
            jQRight_ss2(j) = -(Omega0Tilde(j+1,j)/Mass(j+1))*D_lss_ss2(Nc+j+1,j);
        }
    }

    if(rank == 0)
    {
        cout << "Steady-state heat currents (WBA): \t" << scientific << setprecision(3);
        ofstream ss_heatcurrent("output/heat_current_ss_WBA.out", ios::out);
        ss_heatcurrent << setprecision(5) << scientific;
        for(int j=0; j<Nc-1; j++)
        {
            cout << real(jQRight_ss(j)) << " ";
            ss_heatcurrent << real(jQLeft_ss(j)) << " " << real(jQRight_ss(j)) << " ";
        }
        cout << endl;
        ss_heatcurrent << endl;
        ss_heatcurrent.close();

        cout << "Steady-state heat currents (Exact): \t" << scientific << setprecision(3);
        ofstream ss_heatcurrent2("output/heat_current_ss_exact.out", ios::out);
        ss_heatcurrent2 << setprecision(5) << scientific;
        for(int j=0; j<Nc-1; j++)
        {
            cout << real(jQRight_ss2(j)) << " ";
            ss_heatcurrent2 << real(jQLeft_ss2(j)) << " " << real(jQRight_ss2(j)) << " ";
        }
        cout << endl;
        ss_heatcurrent2 << endl;
        ss_heatcurrent2.close();
    }

    cx_mat temp(2*Nc,2*Nc), tempminus(2*Nc,2*Nc), tempplus(2*Nc,2*Nc);
    temp.zeros();
    tempminus.zeros();
    tempplus.zeros();
    cx_mat ss_temp(2*Nc,2*Nc);
    ss_temp.zeros();
    for(int i=0; i<elems; i++)
    {
        int jj = elems*rank + i;
        for(int j=0; j<Nc-1; j++)
        {
            if(Nc > 1)
            {
                jQLeft(j,i) = (OmegaTilde(j,j+1)/Mass(j))*D_lss(j,Nc+j+1,i);
                jQRight(j,i) = -(OmegaTilde(j+1,j)/Mass(j))*D_lss(Nc+j+1,j,i);
            }
        }

        // Total energy from the trace formula
        EQ(i) = trace(OmegaTilde*D_lss.slice(i));

        // Derivative of the total energy (simple 2-point rule)
        if(i==0) dEQ(i) = (trace(OmegaTilde*D_lss.slice(i+1)) - trace(OmegaTilde*D_lss.slice(i)))/dt;
        else if(i==elems-1) dEQ(i) = (trace(OmegaTilde*D_lss.slice(i)) - trace(OmegaTilde*D_lss.slice(i-1)))/dt;
        else dEQ(i) = (trace(OmegaTilde*D_lss.slice(i+1)) - trace(OmegaTilde*D_lss.slice(i-1)))/(2.0*dt);

        temp = OmegaTilde*D_lss.slice(i);
        if(i>0) tempminus = OmegaTilde*D_lss.slice(i-1);
        if(i<elems-1) tempplus = OmegaTilde*D_lss.slice(i+1);
        for(unsigned int j=0; j<Nc; j++)
        {
            LocalT(j,i) = (temp(j,j)+temp(j+Nc,j+Nc))*0.5;
            // Derivative of the local temperature
            if(i==0)
            {
                dLocalT(j,i) = ((tempplus(j,j)+tempplus(j+Nc,j+Nc)) - (temp(j,j)+temp(j+Nc,j+Nc)))/dt;
            }
            else if(i==elems-1)
            {
                dLocalT(j,i) = ((temp(j,j)+temp(j+Nc,j+Nc)) - (tempminus(j,j)+tempminus(j+Nc,j+Nc)))/dt;
            }
            else 
            {
                dLocalT(j,i) = ((tempplus(j,j)+tempplus(j+Nc,j+Nc)) - (tempminus(j,j)+tempminus(j+Nc,j+Nc)))/(2.0*dt);
            }

            for(unsigned int k=0; k<Nc; k++)
            {
                if(j==0)
                {
                    tmpcdble = cx_double(0.0);
                }
                else if(j==Nc-1)
                {
                    tmpcdble = cx_double(0.0);
                }
                else tmpcdble += OmegaTilde(j,k)*D_lss.slice(i)(j,k);
            }
            Eloc(j,i) = 0.5*(D_lss.slice(i)(j+Nc,j+Nc) + tmpcdble);
            tmpcdble = cx_double(0.0,0.0);
        }
        if(rank == 0)
        {
            ss_temp = OmegaTilde*D_lss_ss;
            ofstream ss_loct("output/ss_local_temperature_wba.out", ios::out);
            ss_loct << setprecision(5) << scientific;
            for(unsigned int j=0; j<Nc; j++)
            {
                ss_loct << real((ss_temp(j,j)+ss_temp(j+Nc,j+Nc))*0.5) << " ";
            }
            ss_loct << endl;
            ss_loct.close();

            ss_temp = OmegaTilde*D_lss_ss2;
            ofstream ss_loct2("output/ss_local_temperature_exact.out", ios::out);
            ss_loct2 << setprecision(5) << scientific;
            for(unsigned int j=0; j<Nc; j++)
            {
                ss_loct2 << real((ss_temp(j,j)+ss_temp(j+Nc,j+Nc))*0.5) << " ";
            }
            ss_loct2 << endl;
            ss_loct2.close();
        }
    }
}
