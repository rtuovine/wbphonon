SRCDIR = src
OBJDIR = obj
SOURCE = $(wildcard $(SRCDIR)/*.cpp)
OBJECT = $(addprefix $(OBJDIR)/,$(notdir $(SOURCE:.cpp=.o)))
EXECUTABLE = wbphonon

.PHONY: default desktop laptop mpsd electra taito sisu hydra wbphonon clean

default:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings";\
	lflags="-larmadillo -lgsl -lgslcblas";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

desktop:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings";\
	lflags="-larmadillo -lgsl -lgslcblas";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

laptop:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings";\
	lflags="-larmadillo -lgsl -lgslcblas";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

mpsd:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings -I/home/rtuovin/localprogs/armadillo-6.100.0/usr/include";\
	lflags="-llapack -lblas -lgsl -lgslcblas -L/home/rtuovin/localprogs/armadillo-6.100.0/usr/lib -larmadillo";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

electra:
	@cxx="mpiCC";\
	cflags="-c -O3 -Wno-write-strings -I/work/rimasatu/lib/armadillo-6.100.0/include";\
	lflags="-L/usr/lib64/ -lgsl -lgslcblas -L/work/rimasatu/lib/armadillo-6.100.0 -larmadillo";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

taito:
	@cxx="mpiCC";\
	cflags="-c -O3 -Wno-write-strings -xHost -opt-prefetch -unroll-aggressive -no-prec-div -fp-model fast=2 -I/homeappl/home/rtuovine/appl_taito/armadillo-5.100.2/include";\
	lflags="-liomp5 -lpthread -L/usr/lib64/atlas-sse3/ -lcblas -L/usr/lib64/atlas -llapack -L/homeappl/home/rtuovine/appl_taito/armadillo-5.100.2/usr/lib64/ -larmadillo -L/homeappl/home/rtuovine/appl_taito/gsl-1.16/lib -lgsl -lgslcblas";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

sisu:
	@cxx="CC";\
	cflags="-c -O3 -Wno-write-strings -xHost -opt-prefetch -unroll-aggressive -no-prec-div -fp-model fast=2 -I/homeappl/home/rtuovine/appl_sisu/armadillo-5.100.2/include -I/homeappl/home/rtuovine/appl_taito/gsl-1.16/include -I/opt/intel/composer_xe_2015.2.164/mkl/include";\
	lflags="-liomp5 -lpthread -dynamic -L/opt/intel/composer_xe_2015.2.164/mkl/lib/intel64 -lmkl_blas95_lp64 -lmkl_lapack95_lp64 -lmkl_sequential -lmkl_core -lmkl_intel_lp64 -lmkl_intel_thread -L/homeappl/home/rtuovine/appl_sisu/armadillo-5.100.2/usr/lib64/ -larmadillo -L/homeappl/home/rtuovine/appl_taito/gsl-1.16/lib -lgsl -lgslcblas ";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

hydra:
	@cxx="mpiicc";\
	cflags="-c -O3 -Wno-write-strings -xHost -qopt-prefetch -unroll-aggressive -no-prec-div -fp-model fast=2 -I/u/rtuovin/armadillo-6.100.0/usr/include -I/u/rtuovin/gsl-2.2/include -I/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/include";\
	lflags="-liomp5 -lpthread -L/u/system/SLES11/soft/intel16.3/16.0/linux/mkl/lib/intel64 -lmkl_blas95_lp64 -lmkl_lapack95_lp64 -lmkl_sequential -lmkl_core -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_rt -L/u/rtuovin/armadillo-6.100.0/usr/lib64 -larmadillo -L/u/rtuovin/gsl-2.2/lib -lgsl -lgslcblas ";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

helsinki:
	@cxx="mpiCC";\
	cflags="-g -c -O3 -Wno-write-strings -I/home/rikutuov/Downloads/armadillo-6.100.0/local/usr/include";\
	lflags="-llapack -lblas -lgsl -lgslcblas -L/home/rikutuov/Downloads/armadillo-6.100.0/local/usr/lib -larmadillo";\
	$(MAKE) CXX="$$cxx" CFLAGS="$$cflags" LDFLAGS="$$lflags" $(EXECUTABLE)

$(EXECUTABLE): $(OBJECT)
	@echo ""
	@echo "Building the target $@ ..."
	$(CXX) $(OBJECT) $(LDFLAGS) -o $(EXECUTABLE)
	@echo ""
	@echo "Build complete."
	@echo ""

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@echo ""
	@echo "Compiling $< ..."
	$(CXX) $(CFLAGS) $< -o $@

clean:
	rm -rf $(OBJDIR)/*.o $(EXECUTABLE)
	rm -f output/*.out
