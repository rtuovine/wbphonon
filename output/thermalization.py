#!/usr/bin/python
import numpy as np
import pylab as pyl
import matplotlib.pyplot as plt
from matplotlib import rc
import sys

td_data_left = np.genfromtxt('heat_current_left.out', dtype=None)
td_data_right = np.genfromtxt('heat_current_right.out', dtype=None)
ss_data = np.genfromtxt('heat_current_ss_WBA.out', dtype=None)

nb = len(ss_data)/2                             # Number of bonds
nt = len(td_data_left)                          # Number of time steps
dt = abs(td_data_left[0,0] - td_data_left[1,0]) # Time step length
dJ = float(sys.argv[1])                         # Saturation ratio

print ""
print "Number of bonds:\t",nb
print "Number of time steps:\t",nt
print "Time step length:\t",dt
print ""

der_left = np.diff(np.delete(td_data_left, 0, 1),axis=0)/dt
der_right = np.diff(np.delete(td_data_right, 0, 1),axis=0)/dt

thermalized_left = np.zeros(nb, dtype = bool)
thermalized_right = np.zeros(nb, dtype = bool)
T_left = np.zeros(nb)
T_right = np.zeros(nb)

ftherm  = open("therm_time.out", "w")

for j in range(nb):
    if(abs(ss_data[2*j]) < 1.0e-12 or abs(ss_data[2*j+1]) < 1.0e-10): # Special case when steady-state values are close to zero
        for k in range(nt-1):
            if(abs(((1.0+td_data_left[k,j+1]) - (1.0+ss_data[2*j]))/(1.0+ss_data[2*j])) <= dJ and
               abs(td_data_left[k+1,j+1] - td_data_left[k,j+1])/dt <= dJ*max(abs(der_left[:,j])) and
               thermalized_left[j] == False):
                T_left[j] = float(k*dt)
                print "Left heat current in bond", j+1, "saturated at T =", T_left[j], ". J_L^Q(T) =", td_data_left[k,j+1], "( steady-state =", ss_data[2*j], ")"
                thermalized_left[j] = True
            if(abs(((1.0+td_data_right[k,j+1]) - (1.0+ss_data[2*j+1]))/(1.0+ss_data[2*j+1])) <= dJ and
               abs(td_data_right[k+1,j+1] - td_data_right[k,j+1])/dt <= dJ*max(abs(der_right[:,j])) and
               thermalized_right[j] == False):
                T_right[j] = float(k*dt)
                if(j == (nb-1)/2):
                    ftherm.write(str(T_right[j]))
                print "Right heat current in bond", j+1, "saturated at T =", T_right[j], ". J_R^Q(T) =", td_data_right[k,j+1], "( steady-state =", ss_data[2*j+1], ")"
                thermalized_right[j] = True
                print ""
    else:
        for k in range(nt):
            if(abs((td_data_left[k,j+1] - ss_data[2*j])/ss_data[2*j]) <= dJ and
               abs(td_data_left[k,j+1] - td_data_left[k-1,j+1])/dt <= dJ*max(abs(der_left[:,j])) and
               thermalized_left[j] == False):
                T_left[j] = float(k*dt)
                print "Left heat current in bond", j+1, "saturated at T =", T_left[j], ". J_L^Q(T) =", td_data_left[k,j+1], "( steady-state =", ss_data[2*j], ")"
                thermalized_left[j] = True
            if(abs((td_data_right[k,j+1] - ss_data[2*j+1])/ss_data[2*j+1]) <= dJ and
               abs(td_data_right[k,j+1] - td_data_right[k-1,j+1])/dt <= dJ*max(abs(der_right[:,j])) and
               thermalized_right[j] == False):
                T_right[j] = float(k*dt)
                if(j == (nb-1)/2):
                    ftherm.write(str(T_right[j]))
                print "Right heat current in bond", j+1, "saturated at T =", T_right[j], ". J_R^Q(T) =", td_data_right[k,j+1], "( steady-state =", ss_data[2*j+1], ")"
                thermalized_right[j] = True
                print ""        

ftherm.close()

fig_width_pt  = 250.0                       # Get this from LaTeX using \showthe\columnwidth
inches_per_pt = 1.0/72.27                   # Convert pt to inches
golden_mean   = (pyl.sqrt(5)-1.0)/2.0       # Aesthetic ratio
fig_width     = fig_width_pt*inches_per_pt  # width in inches
fig_height    = fig_width*golden_mean       # height in inches
fig_size      = [fig_width,fig_height]

params = {'backend': 'pdf',                      # ps pdf svg
          'axes.labelsize': 8,
          'text.fontsize': 8,
          'legend.fontsize': 6,
          'legend.fancybox': True,
          'xtick.labelsize': 6,
          'xtick.major.size': 4,
          'xtick.minor.size': 2,
          'xtick.major.width': 0.5,
          'xtick.minor.width': 0.5,
          'xtick.major.pad': 4,
          'xtick.minor.pad': 4,
          'ytick.labelsize': 6,
          'ytick.major.size': 4,
          'ytick.minor.size': 2,
          'ytick.major.width': 0.5,
          'ytick.minor.width': 0.5,
          'ytick.major.pad': 4,
          'ytick.minor.pad': 4,
          'text.usetex': True,
          'ps.usedistiller': 'xpdf',            # None ghostscript xpdf
          'lines.linewidth': 1.0,
          'lines.dash_capstyle': 'butt',        # butt round projecting
          'lines.dash_joinstyle': 'miter',      # miter round bevel
          'figure.figsize': fig_size,
          'SubplotParams.hspace': 0}

short_dash = (2.0,2.5)
long_dash = (4.5,1.5)
dash_dot = [4, 2, 1, 2, 1, 2]
mygreen = (0.0,0.5,0.0)
mymagenta = (0.75,0.0,0.75)
mycyan = (0.0,0.75,0.75)

plt.close('all')

pyl.rcParams.update(params)
rc('text.latex', preamble = \
    '\usepackage{amsmath},' \
    '\usepackage{amssymb},' \
    '\usepackage{upgreek},'
    '\usepackage{color},' )

fig = pyl.figure()
fig.clf()
ax = pyl.axes()

if(nb == 1):
    pyl.plot(td_data_right[:,0], td_data_right[:,1], '-', color=(1.0,0.0,0.0), label=r"Bond 1", linewidth=1.0)
    pyl.axhline(y = ss_data[1], color=(1.0,0.0,0.0), dashes=short_dash)
    pyl.axvline(x = T_right[0], color=(1.0,0.0,0.0))
if(nb == 2):
    pyl.plot(td_data_right[:,0], td_data_right[:,1], '-', color=(1.0,0.0,0.0), label=r"Bond 1", linewidth=1.0)
    pyl.plot(td_data_right[:,0], td_data_right[:,2], '-', color=mygreen, label=r"Bond 2", dashes=long_dash, linewidth=1.0)
    pyl.axhline(y = ss_data[1], color=(1.0,0.0,0.0), dashes=short_dash)
    pyl.axhline(y = ss_data[3], color=mygreen, dashes=short_dash)
    pyl.axvline(x = T_right[0], color=(1.0,0.0,0.0))
    pyl.axvline(x = T_right[1], color=mygreen)
if(nb >= 3):
    pyl.plot(td_data_right[:,0], td_data_right[:,1], '-', color=(1.0,0.0,0.0), label=r"Bond 1", linewidth=1.0)
    pyl.plot(td_data_right[:,0], td_data_right[:,2], '-', color=mygreen, label=r"Bond 2", dashes=long_dash, linewidth=1.0)
    pyl.plot(td_data_right[:,0], td_data_right[:,3], '-', color=(0.0,0.0,1.0), label=r"Bond 3", dashes=dash_dot, linewidth=1.0)
    pyl.axhline(y = ss_data[1], color=(1.0,0.0,0.0), dashes=short_dash)
    pyl.axhline(y = ss_data[3], color=mygreen, dashes=short_dash)
    pyl.axhline(y = ss_data[5], color=(0.0,0.0,1.0), dashes=short_dash)
    pyl.axvline(x = T_right[0], color=(1.0,0.0,0.0))
    pyl.axvline(x = T_right[1], color=mygreen)
    pyl.axvline(x = T_right[2], color=(0.0,0.0,1.0))

pyl.legend(loc='upper right', ncol=1, shadow=True)
pyl.xlabel(r'$t$')
pyl.ylabel(r'$J_R^Q(t)$')
pyl.tight_layout()
pyl.show()
