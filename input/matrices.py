#!/usr/bin/python
from ase.visualize import view
from ase.structure import graphene_nanoribbon
from ase.io import read, write
from ase.atoms import Atoms, string2symbols
from ase.data import covalent_radii
from ase.utils import gcd
import numpy as np
from numpy import sqrt, cos, pi, exp
import sys
import warnings

#    python matrices.py z 1 1 1 1.0 1.0 0.1 1

# System
systype         = str(sys.argv[1])      # 'z' 'a'
width           = int(sys.argv[2])      # 3
length          = int(sys.argv[3])      # 14
show            = int(sys.argv[4])      # 1
kC              = float(sys.argv[5])    # 1.0
klambda         = float(sys.argv[6])    # 1.0
kClambda        = float(sys.argv[7])    # 0.1
terminal        = int(sys.argv[8])      # 0

# Get structure
if(terminal == 0):
    if(systype == 'a'):
        gnr = graphene_nanoribbon(width, length, type='armchair', saturated=False)
    elif(systype == 'z'):
        gnr = graphene_nanoribbon(width, length, type='zigzag', saturated=False)
elif(terminal == 1):
    if(systype == 'a'):
        gnr = graphene_nanoribbon(width, length+1, type='armchair', saturated=False)
    elif(systype == 'z'):
        gnr = graphene_nanoribbon(width, length+1, type='zigzag', saturated=False)
else:
    print 'Terminal site in the central region: choose either 0 or 1'
    exit(1)

# Get coordinates
pos = gnr.get_positions()
N = 2*len(pos)
delta = 1.0e-2

# View structure
if(show == 1):
    view(gnr)

# Function for calculating distances
def dist(j,k):
    return sqrt(  (pos[j][0]-pos[k][0])**2
                + (pos[j][1]-pos[k][1])**2
                + (pos[j][2]-pos[k][2])**2 )

# Hamiltonian matrix

Omega0Re  = [[0.0 for j in range(N)] for k in range(N)]
Omega0Im  = [[0.0 for j in range(N)] for k in range(N)]
OmegaRe   = [[0.0 for j in range(N)] for k in range(N)]
OmegaIm   = [[0.0 for j in range(N)] for k in range(N)]
alphaRe   = [[0.0 for j in range(N)] for k in range(N)]
alphaIm   = [[0.0 for j in range(N)] for k in range(N)]
LambdaLRe = [[0.0 for j in range(N)] for k in range(N)]
LambdaLIm = [[0.0 for j in range(N)] for k in range(N)]
LambdaRRe = [[0.0 for j in range(N)] for k in range(N)]
LambdaRIm = [[0.0 for j in range(N)] for k in range(N)]
GammaLRe  = [[0.0 for j in range(N)] for k in range(N)]
GammaLIm  = [[0.0 for j in range(N)] for k in range(N)]
GammaRRe  = [[0.0 for j in range(N)] for k in range(N)]
GammaRIm  = [[0.0 for j in range(N)] for k in range(N)]
Mass      = [0.0 for j in range(N/2)]
OnSite    = [0.0 for j in range(N/2)]

# Open file for writing the matrix
fOmega0  = open("Omega0.in", "w")
fOmega   = open("Omega.in", "w")
falpha   = open("alpha.in", "w")
fLambdaL = open("LambdaLeft.in", "w")
fLambdaR = open("LambdaRight.in", "w")
fGammaL  = open("GammaLeft.in", "w")
fGammaR  = open("GammaRight.in", "w")
fMass    = open("Mass.in", "w")
fOnSite  = open("OnSite.in", "w")

# Loop over the matrix dimensions
for j in range(N):
    if(j < N/2):
        if(j == 1):
            Mass[j] = 1.0
        else:
            Mass[j] = 1.0
        fMass.write(str(j) + "\t" + str(Mass[j]) + "\n")
        if(j == 0 or j == 1):
            OnSite[j] = 0.0
        else:
            OnSite[j] = 0.0
        fOnSite.write(str(j) + "\t" + str(OnSite[j]) + "\n")

for j in range(N):
    if(j < N/2):
        if(abs(pos[j][2] - min(pos[:,2])) < delta):
            if(terminal == 0):
                LambdaLRe[j][j] = -kClambda**2/klambda
                GammaLRe[j][j]  = 2.0*kClambda**2/(klambda**1.5)
            elif(terminal == 1):
                LambdaLRe[j][j] = -klambda
                GammaLRe[j][j]  = 2.0*sqrt(klambda)
            else:
                print 'Unknown terminal parameter, exiting...'
                exit(1)
        if(abs(pos[j][2] - max(pos[:,2])) < delta):
            if(terminal == 0):
                LambdaRRe[j][j] = -kClambda**2/klambda
                GammaRRe[j][j]  = 2.0*kClambda**2/(klambda**1.5)
            elif(terminal == 1):
                LambdaRRe[j][j] = -klambda
                GammaRRe[j][j]  = 2.0*sqrt(klambda)
            else:
                print 'Unknown terminal parameter, exiting...'
                exit(1)
    for k in range(N):
        if(terminal == 0):
            if(j < N/2 and k < N/2):
                if( (abs(pos[j][2] - min(pos[:,2])) < delta or abs(pos[k][2] - min(pos[:,2])) < delta) 
                   or (abs(pos[j][2] - max(pos[:,2])) < delta or abs(pos[k][2] - max(pos[:,2])) < delta) ):
                    if(j!=k):
                        if(dist(j,k) < 1.43):
                            Omega0Re[j][k] = -kC
                            OmegaRe[j][k] = -kC/(sqrt(Mass[j])*sqrt(Mass[k]))
                    else:
                        Omega0Re[j][k] = kClambda + kC + OnSite[j]
                        OmegaRe[j][k] = (kClambda + kC + OnSite[j])/(sqrt(Mass[j])*sqrt(Mass[k]))
                else:
                    if(j!=k):
                        if(dist(j,k) < 1.43):
                            Omega0Re[j][k] = -kC
                            OmegaRe[j][k] = -kC/(sqrt(Mass[j])*sqrt(Mass[k]))
                    else:
                        Omega0Re[j][k] = 2.0*kC + OnSite[j]
                        OmegaRe[j][k] = (2.0*kC + OnSite[j])/(sqrt(Mass[j])*sqrt(Mass[k]))
            if(j >= N/2 and k >= N/2):
                if(j == k):
                    OmegaRe[j][k] = 1.0
        elif(terminal == 1):
            if(j < N/2 and k < N/2):
                if( (abs(pos[j][2] - min(pos[:,2])) < delta or abs(pos[k][2] - min(pos[:,2])) < delta) 
                   or (abs(pos[j][2] - max(pos[:,2])) < delta or abs(pos[k][2] - max(pos[:,2])) < delta) ):
                    if(j!=k):
                        if(dist(j,k) < 1.43):
                            Omega0Re[j][k] = -kClambda
                            OmegaRe[j][k] = -kClambda/(sqrt(Mass[j])*sqrt(Mass[k]))
                    else:
                        Omega0Re[j][k] = klambda + kClambda + OnSite[j]
                        OmegaRe[j][k] = (klambda + kClambda + OnSite[j])/(sqrt(Mass[j])*sqrt(Mass[k]))
                elif( (abs(pos[j][2] - (min(pos[:,2])+1.23)) < delta or abs(pos[k][2] - (min(pos[:,2])+1.23)) < delta) 
                   or (abs(pos[j][2] - (max(pos[:,2])-1.23)) < delta or abs(pos[k][2] - (max(pos[:,2])-1.23)) < delta) ):
                    if(j!=k):
                        if(dist(j,k) < 1.43 ):
                            Omega0Re[j][k] = -kC
                            OmegaRe[j][k] = -kC/(sqrt(Mass[j])*sqrt(Mass[k]))
                    else:
                        Omega0Re[j][k] = kClambda + kC + OnSite[j]
                        OmegaRe[j][k] = (kClambda + kC + OnSite[j])/(sqrt(Mass[j])*sqrt(Mass[k]))
                else:
                    if(j!=k):
                        if(dist(j,k) < 1.43 ):
                            Omega0Re[j][k] = -kC
                            OmegaRe[j][k] = -kC/(sqrt(Mass[j])*sqrt(Mass[k]))
                    else:
                        Omega0Re[j][k] = 2.0*kC + OnSite[j]
                        OmegaRe[j][k] = (2.0*kC + OnSite[j])/(sqrt(Mass[j])*sqrt(Mass[k]))

            if(j >= N/2 and k >= N/2):
                if(j == k):
                    Omega0Re[j][k] = 1.0
                    OmegaRe[j][k] = 1.0
        else:
            print 'Unknown terminal parameter, exiting...'
            exit(1)

        if(j >= N/2 and k <= N/2):
            if(j >= k + N/2 and j <= k + N/2):
                alphaIm[j][k] = -1.0
        if(j <= N/2 and k >= N/2):
            if(k >= j + N/2 and k <= j + N/2):
                alphaIm[j][k] = 1.0

        fOmega0.write(str(j) + "\t" + str(k) + "\t" + str(Omega0Re[j][k]) + "\t" + str(Omega0Im[j][k]) + "\n")
        fOmega.write(str(j) + "\t" + str(k) + "\t" + str(OmegaRe[j][k]) + "\t" + str(OmegaIm[j][k]) + "\n")
        falpha.write(str(j) + "\t" + str(k) + "\t" + str(alphaRe[j][k]) + "\t" + str(alphaIm[j][k]) + "\n")
        fLambdaL.write(str(j) + "\t" + str(k) + "\t" + str(LambdaLRe[j][k]) + "\t" + str(LambdaLIm[j][k]) + "\n")
        fLambdaR.write(str(j) + "\t" + str(k) + "\t" + str(LambdaRRe[j][k]) + "\t" + str(LambdaRIm[j][k]) + "\n")
        fGammaL.write(str(j) + "\t" + str(k) + "\t" + str(GammaLRe[j][k]) + "\t" + str(GammaLIm[j][k]) + "\n")
        fGammaR.write(str(j) + "\t" + str(k) + "\t" + str(GammaRRe[j][k]) + "\t" + str(GammaRIm[j][k]) + "\n")

# Close the file
fOmega0.close()
fOmega.close()
falpha.close()
fLambdaL.close()
fLambdaR.close()
fGammaL.close()
fGammaR.close()
fMass.close()
fOnSite.close()

import pandas as pd
pd.options.display.float_format = '{:,.2f}'.format
print ''
print 'Omega0 = '
print pd.DataFrame(Omega0Re)
print ''
print 'Omega = '
print pd.DataFrame(OmegaRe)
print ''
print 'alpha = i*'
print pd.DataFrame(alphaIm)
print ''
print 'LambdaL = '
print pd.DataFrame(LambdaLRe)
print ''
print 'LambdaR = '
print pd.DataFrame(LambdaRRe)
print ''
print 'GammaL = '
print pd.DataFrame(GammaLRe)
print ''
print 'GammaR = '
print pd.DataFrame(GammaRRe)

# Print the size of the ribbon in nm
if(show == 1):
    print 'Dimensions (nm): ', (max(pos[:,0])-min(pos[:,0]))/10, '', (max(pos[:,1])-min(pos[:,1]))/10, '', (max(pos[:,2])-min(pos[:,2]))/10
    
fs = open("size", "w")
fs.write("Dimensions (nm): " + str((max(pos[:,0])-min(pos[:,0]))/10) + " " + str((max(pos[:,1])-min(pos[:,1]))/10) + " " + str((max(pos[:,2])-min(pos[:,2]))/10) + "\n")
fs.close()

# Write the structure to a file
write('structure.xyz', gnr)
